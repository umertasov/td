<?php
namespace App\Entities;

class FileEntity
{
    public $id, $path, $thumb, $filename, $mimetype;

    public function __construct($id, $path, $thumb = '', $filename = '', $mimetype = '')
    {
        $this->id = $id;
        $this->path = config('app.url')  . '/' . $path;
        $this->thumb = config('app.url')  . '/' . $thumb;
        $this->filename = $filename;
        $this->mimetype = $mimetype;
    }
}
