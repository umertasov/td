<?php

/**
 * Глобальные функции, которые используется в разных частях кода.
 */

if (!function_exists('getFilesUrl')) {
    /**
     * Возвращает полный путь к файлу
     *
     * @param $path
     * @return string
     */
    function getFilesUrl($path)
    {
        return config('app.files_url') . $path;
    }
}

if (!function_exists('dotToComma')) {
    /**
     * Проверяет доступность URL
     *
     * @param $number
     * @return string
     */
    function dotToComma($number)
    {
        return str_replace('.', ',', $number);
    }
}

if (!function_exists('urlExist')) {
    /**
     * Проверяет доступность URL
     *
     * @param $url
     * @return bool
     */
    function urlExist($url)
    {
        $headers = get_headers($url);
        return stripos($headers[0], "200 OK") ? true : false;
    }
}

if (!function_exists('numberSpaceFormat')) {
    /**
     * Разбивает число на разряды, отбрасывая округление
     *
     * @param null $number
     * @param int $round
     * @return string
     */
    function numberSpaceFormat($number = null, $round = 2)
    {
        $number_str = number_format($number, $round, '.', ' ');
        $number_arr = explode(".", $number_str);
        if (!isset($number_arr[1]) || $number_arr[1] == 0) {
            return $number_arr[0];
        } else {
            return $number_arr[0] . '.' . $number_arr[1];
        }
    }
}

if (!function_exists('sumToStr')) {
    /**
     * Возвращает денежную сумму прописью
     *
     * @param $num
     * @param bool $money
     * @param bool $kopeiki
     * @return string
     */
    function sumToStr($num, $money = true, $kopeiki = true)
    {
        $nul = 'ноль';
        $ten = [
            ['', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
            ['', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
        ];

        $a20 = [
            'десять',
            'одиннадцать',
            'двенадцать',
            'тринадцать',
            'четырнадцать',
            'пятнадцать',
            'шестнадцать',
            'семнадцать',
            'восемнадцать',
            'девятнадцать'
        ];

        $tens = [
            2 => 'двадцать',
            'тридцать',
            'сорок',
            'пятьдесят',
            'шестьдесят',
            'семьдесят',
            'восемьдесят', 'девяносто'
        ];

        $hundred = [
            '',
            'сто',
            'двести',
            'триста',
            'четыреста',
            'пятьсот',
            'шестьсот',
            'семьсот',
            'восемьсот',
            'девятьсот'
        ];

        if ($money === true) {
            $unit = [
                ['копейка', 'копейки', 'копеек', 1],
                ['рубль', 'рубля', 'рублей', 0],
                ['тысяча', 'тысячи', 'тысяч', 1],
                ['миллион', 'миллиона', 'миллионов', 0],
                ['миллиард', 'милиарда', 'миллиардов', 0],
            ];
        } else {
            $unit = [
                ['', '', '', 1],
                ['', '', '', 0],
                ['тысяча', 'тысячи', 'тысяч', 1],
                ['миллион', 'миллиона', 'миллионов', 0],
                ['миллиард', 'милиарда', 'миллиардов', 0],
            ];
        }

        //
        list($rub, $kop) = explode('.', sprintf("%015.2f", floatval($num)));
        $out = [];
        if (intval($rub) > 0) {
            foreach (str_split($rub, 3) as $uk => $v) { // by 3 symbols
                if (!intval($v)) {
                    continue;
                }
                $uk = sizeof($unit) - $uk - 1; // unit key
                $gender = $unit[$uk][3];
                list($i1, $i2, $i3) = array_map('intval', str_split($v, 1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2 > 1) {
                    $out[] = $tens[$i2] . ' ' . $ten[$gender][$i3]; # 20-99
                } else {
                    $out[] = $i2 > 0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                }
                // units without rub & kop
                if ($uk > 1) {
                    $out[] = morph($v, $unit[$uk][0], $unit[$uk][1], $unit[$uk][2]);
                }
            } //foreach
        } else {
            $out[] = $nul;
        }

        $out[] = morph(intval($rub), $unit[1][0], $unit[1][1], $unit[1][2]); // rub

        if ($kopeiki == true) {
            $out[] = $kop . ' ' . morph($kop, $unit[0][0], $unit[0][1], $unit[0][2]); // kop
        }
        return trim(preg_replace('/ {2,}/', ' ', join(' ', $out)));
    }
}

if (!function_exists('morph')) {
    /**
     * Склоняет словоформу
     *
     * @param $n
     * @param $f1
     * @param $f2
     * @param $f5
     * @return mixed
     */
    function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) {
            return $f5;
        }
        $n = $n % 10;
        if ($n > 1 && $n < 5) {
            return $f2;
        }
        if ($n == 1) {
            return $f1;
        }
        return $f5;
    }
}

if (!function_exists('rdate')) {
    /**
     * Преобразует дату из unixtime в формат "1 январ[я] 2016"
     *      $format:
     *          F - полное название месяца (январь);
     *          M - сокращенное название месяца (янв);
     *      $case:
     *          0 - именительный падеж (январь);
     *          1 - родительный (января);
     *          2 - предложный (январе);
     *          3 - дательный (январю);
     *          4 - творительный (январем);
     *
     * @param $format
     * @param null $timestamp
     * @param int $case
     * @return false|string
     */
    function rdate($format, $timestamp = null, $case = 0)
    {
        if ($timestamp === null) {
            $timestamp = time();
        }

        static $loc =
            'январ,ь,я,е,ю,ём,е
          феврал,ь,я,е,ю,ём,е
          март, ,а,е,у,ом,е
          апрел,ь,я,е,ю,ем,е
          ма,й,я,е,ю,ем,е
          июн,ь,я,е,ю,ем,е
          июл,ь,я,е,ю,ем,е
          август, ,а,е,у,ом,е
          сентябр,ь,я,е,ю,ём,е
          октябр,ь,я,е,ю,ём,е
          ноябр,ь,я,е,ю,ём,е
          декабр,ь,я,е,ю,ём,е';

        if (is_string($loc)) {
            $months = array_map('trim', explode("\n", $loc));
            $loc = [];
            foreach ($months as $monthLocale) {
                $cases = explode(',', $monthLocale);
                $base = array_shift($cases);

                $cases = array_map('trim', $cases);

                $loc[] = [
                    'base' => $base,
                    'cases' => $cases,
                ];
            }
        }

        $m = (int)date('n', $timestamp) - 1;

        $F = $loc[$m]['base'] . $loc[$m]['cases'][$case];

        $format = strtr($format, [
            'F' => $F,
            'M' => substr($F, 0, 3),
        ]);

        return date($format, $timestamp);
    }
}

if (!function_exists('transliterate')) {
    /**
     * Транслитерация кириллицы в латиницу
     *
     * @param string $text
     * @return false|mixed|null|string|string[]
     */
    function transliterate(string $text = "")
    {
        $tr = [
            "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D", "Е" => "E", "Ё" => "E", "Ж" => "J",
            "З" => "Z", "И" => "I", "Й" => "Y", "К" => "K", "Л" => "L", "М" => "M", "Н" => "N", "О" => "O",
            "П" => "P", "Р" => "R", "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "H", "Ц" => "TS",
            "Ч" => "CH", "Ш" => "SH", "Щ" => "SCH", "Ъ" => "", "Ы" => "YI", "Ь" => "", "Э" => "E", "Ю" => "YU",
            "Я" => "YA", "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "e",
            "ж" => "j", "з" => "z", "и" => "i", "й" => "y", "к" => "k", "л" => "l", "м" => "m", "н" => "n",
            "о" => "o", "п" => "p", "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "h",
            "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch", "ъ" => "y", "ы" => "yi", "ь" => "", "э" => "e",
            "ю" => "yu", "я" => "ya", "«" => "", "»" => "", "№" => "", "Ӏ" => "", "’" => "", "ˮ" => "", "_" => "-",
            "'" => "", "`" => "", "^" => "", "\." => "", "," => "", ":" => "", "<" => "", ">" => "", "!" => ""
        ];
        //Транслитерация
        foreach ($tr as $ru => $en) {
            $text = mb_eregi_replace($ru, $en, $text);
        }
        $text = mb_strtolower($text);
        $text = str_replace(' ', '-', $text);
        return $text;
    }
}

if (!function_exists('getTitleMonth')) {
    /**
     * Возвращает название месяца на русском
     *
     * @param null $number
     * @return bool|string
     */
    function getTitleMonth($number = null)
    {
        switch ($number) {
            case 1:
                return 'Январь';
            case 2:
                return 'Февраль';
            case 3:
                return 'Март';
            case 4:
                return 'Апрель';
            case 5:
                return 'Май';
            case 6:
                return 'Июнь';
            case 7:
                return 'Июль';
            case 8:
                return 'Август';
            case 9:
                return 'Сентябрь';
            case 10:
                return 'Октябрь';
            case 11:
                return 'Ноябрь';
            case 12:
                return 'Декабрь';
            default:
                return false;
        }
    }
}

if (!function_exists('phoneParse')) {
    /**
     * Приведение номера телефона к правильному виду
     *
     * @param string $phone
     * @return null|string|string[]
     */
    function phoneParse($phone = "")
    {
        $phone = preg_replace("/[^0-9]/", '', $phone);
        $replace = preg_replace('/^([8]{1})([0-9]{10}$)/', '7$2', $phone);
        if ($replace != "") {
            $phone = $replace;
        }
        return $phone;
    }
}

if (!function_exists('hidePhone')) {
    /**
     * Скрытие телефонного номера
     *
     * @param $phone
     * @return mixed
     */
    function hidePhone($phone)
    {
        return substr_replace($phone, "*****", 4, 5);
    }
}

if (!function_exists('ruMonth')) {
    /**
     * Перевод названий месяцев с английского на русский
     *
     * @param $en
     * @return string
     */
    function ruMonth($en)
    {
        switch ($en) {
            case "January":
                return "Января";
            case "February":
                return "Февраля";
            case "March":
                return "Марта";
            case "April":
                return "Апреля";
            case "May":
                return "Мая";
            case "June":
                return "Июня";
            case "July":
                return "Июля";
            case "August":
                return "Августа";
            case "September":
                return "Сентября";
            case "October":
                return "Октября";
            case "November":
                return "Ноября";
            case "December":
                return "Декабря";
            default:
                return "";
        }
    }
}

if (!function_exists('generatePassword')) {
    /**
     * Генерирование пароля
     *
     * @param int $number
     * @param bool $numbers
     * @param bool $marks
     * @return string
     */
    function generatePassword($number = 6, $numbers = true, $marks = true)
    {
        $symbols = [
            'a', 'b', 'c', 'd', 'e', 'f',
            'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'o', 'p', 'r', 's',
            't', 'u', 'v', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F',
            'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'R', 'S',
            'T', 'U', 'V', 'X', 'Y', 'Z'
        ];

        if ($numbers) {
            $symbols = array_merge($symbols, [
                '1', '2', '3', '4',
                '5', '6', '7', '8',
                '9', '0'
            ]);
        }

        if ($marks) {
            $symbols = array_merge($symbols, [
                '.', ',', '(', ')', '[', ']',
                '!', '?', '&', '^', '%', '@',
                '*', '$', '<', '>', '/', '|',
                '+', '-', '{', '}', '`', '~'
            ]);
        }

        $count_symbols = count($symbols);

        $pass = "";
        for ($i = 0; $i < $number; $i++) {
            $index = mt_rand(0, $count_symbols - 1);
            $pass .= $symbols[$index];
        }
        return $pass;
    }
}

if (!function_exists('getCountMonthDays')) {
    /**
     * Возвращает кол-во дней в месяце с учетом високосных
     *
     * @param null $number
     * @param null $year
     * @return int
     */
    function getCountMonthDays($number = null, $year = null)
    {
        $days = 0;
        if ($number != null && $year != null) {
            if ($number == 2) {
                if ($year % 4 == 0) {
                    $days = 29;
                } else {
                    $days = 28;
                }
            } elseif ($number == 1 || $number == 3 || $number == 5 || $number == 7 || $number == 8 || $number == 10 || $number == 12) {
                $days = 31;
            } else {
                $days = 30;
            }
        }
        return $days;
    }
}

if (!function_exists('numberRound')) {
    function numberRound($num, $round)
    {
        return rtrim(rtrim(sprintf("%1.{$round}F", $num), '0'), '.');
    }
}

if (!function_exists('date2str')) {
    function date2str($date)
    {
        if (empty($date)) {
            return '';
        }

        $thousands = [
            1 => 'одна тысяча',
            2 => 'две тысячи',
        ];

        $hundreds = [
            0 => '',
            9 => 'девятьсот',
        ];

        $days = [
            1 => 'первое',
            2 => 'второе',
            3 => 'третье',
            4 => 'четвертое',
            5 => 'пятое',
            6 => 'шестое',
            7 => 'седьмое',
            8 => 'восьмое',
            9 => 'девятое',
            10 => 'десятое',
            11 => 'одиннадцатое',
            12 => 'двенадцатое',
            13 => 'тринадцатое',
            14 => 'четырнадцатое',
            15 => 'пятнадцатое',
            16 => 'шестнадцатое',
            17 => 'семнадцатое',
            18 => 'восемнадцатое',
            19 => 'девятнадцатое',
            20 => 'двадцатое',
            30 => 'тридцатое',
            40 => 'сороковое',
        ];

        $tens = [
            20 => 'двадцать',
            30 => 'тридцать',
            40 => 'сорок',
            40 => 'сорок',
        ];

        foreach ($tens as $d => $ten) {
            for ($day = 1; $day < 10; $day++) {
                $days[$d + $day] = $ten . ' ' . $days[$day];
            }
        }

        $months = [
            0 => 'нулября',
            1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',
        ];

        list($year, $month, $day) = explode('-', $date);

        $monthStr = $months[(int)$month];
        $dayStr = $days[(int)$day];

        $yearPart = $days[(int)mb_substr($year, -2)];
        $endYear = mb_substr($yearPart, -2);

        switch ($endYear) {
            case 'ое':
                $yearPart = mb_substr($yearPart, 0, -2) . 'ого';
                break;
            case 'ье':
                $yearPart .= 'го';
                break;
        }

        $yearParts = [
            $thousands[(int)$year[0]],
            $hundreds[(int)$year[1]],
            $yearPart
        ];

        $yearStr = implode(' ', array_filter(array_map('trim', $yearParts)));

        return implode(' ', [$dayStr, $monthStr, $yearStr]);
    }
}
