<?php

if (!function_exists('jsonSuccess')) {
    function jsonSuccess($value = [])
    {
        return response()->json([
            'status' => 'success',
            'data' => $value
        ], 200, [], JSON_NUMERIC_CHECK);
    }
}

if (!function_exists('jsonSuccessCreate')) {
    function jsonSuccessCreate($value = [])
    {
        return response()->json(array_merge([
            'status' => 'success'
        ], $value), 201, [], JSON_NUMERIC_CHECK);
    }
}

if (!function_exists('jsonAuthorizationError')) {
    function jsonAuthorizationError($value = [], $logMessage = '')
    {
        return response()->json([
            'status' => 'error',
            'errors' => $value
        ], 401, [], JSON_NUMERIC_CHECK);
    }
}

if (!function_exists('jsonAccessError')) {
    function jsonAccessError($value = [], $logMessage = '')
    {
        return response()->json([
            'status' => 'error',
            'errors' => $value
        ], 403, [], JSON_NUMERIC_CHECK);
    }
}

if (!function_exists('jsonNotFound')) {
    function jsonNotFound($value = [], $logMessage = '')
    {
        return response()->json([
            'status' => 'error',
            'errors' => $value
        ], 404, [], JSON_NUMERIC_CHECK);
    }
}

if (!function_exists('jsonValidationError')) {
    function jsonValidationError($value = [], $logMessage = '')
    {
        return response()->json([
            'status' => 'error',
            'errors' => $value
        ], 422, [], JSON_NUMERIC_CHECK);
    }
}

if (!function_exists('jsonServerError')) {
    function jsonServerError($value = [], $logMessage = '')
    {
        return response()->json([
            'status' => 'error',
            'errors' => $value
        ], 500, [], JSON_NUMERIC_CHECK);
    }
}