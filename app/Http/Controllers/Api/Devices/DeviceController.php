<?php

namespace App\Http\Controllers\Api\Devices;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Services\DeviceService;

class DeviceController extends Controller
{
    protected $deviceService;

    /**
     * DeviceController constructor.
     * @param DeviceService $deviceService
     */
    public function __construct(DeviceService $deviceService)
    {
        $this->deviceService = $deviceService;
    }

    public function getDevices()
    {
        $userId = Auth::id();

        $devices = $this->deviceService->getUserDevices($userId);

        return jsonSuccess($devices);
    }

    public function addDevice(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'name' => 'required|string',
            'address' => 'required|string'
        ]);

        $device = $this->deviceService->add([
            'user_id' => $userId,
            'name' => $request->input('name'),
            'address' => $request->input('address'),
        ]);

        return jsonSuccessCreate(['id' => $device->id]);
    }
}