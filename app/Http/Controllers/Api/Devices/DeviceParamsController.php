<?php

namespace App\Http\Controllers\Api\Devices;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\DeviceParamRequest;
use App\Http\Controllers\Controller;
use App\Services\DeviceParamService;
use App\Services\DeviceService;

class DeviceParamsController extends Controller
{
    protected $deviceParamService, $deviceService;

    /**
     * DeviceController constructor.
     * @param DeviceParamService $deviceParamService
     * @param DeviceService $deviceService
     */
    public function __construct(DeviceParamService $deviceParamService, DeviceService $deviceService)
    {
        $this->deviceParamService = $deviceParamService;
        $this->deviceService = $deviceService;
    }

    public function getParams()
    {
        $userId = Auth::id();

        $devices = $this->deviceParamService->getParams($userId);

        return jsonSuccess($devices);
    }

    public function setParams(DeviceParamRequest $request)
    {
        $userId = Auth::id();

        try {
            $this->deviceParamService->edit($userId, [
                'hand_up' => $request->input('hand_up', 0) ?? 0,
                'call' => $request->input('call', 0) ?? 0,
                'sms' => $request->input('sms', 0) ?? 0,
                'heartbeat' => $request->input('heartbeat', 0) ?? 0,
                'theme' => $request->input('theme', 0) ?? 0,
                'whatsapp' => $request->input('whatsapp', 0) ?? 0,
                'viber' => $request->input('viber', 0) ?? 0,
                'vk' => $request->input('vk', 0) ?? 0,
                'facebook' => $request->input('facebook', 0) ?? 0,
                'twitter' => $request->input('twitter', 0) ?? 0,
                'skype' => $request->input('skype', 0) ?? 0,
            ]);

            return jsonSuccess();
        } catch (\Exception $e) {
            return jsonServerError();
        }
    }

}