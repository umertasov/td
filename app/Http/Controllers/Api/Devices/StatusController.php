<?php

namespace App\Http\Controllers\Api\Devices;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\DeviceStatusRequest;
use App\Services\DeviceService;

class StatusController extends Controller
{
    protected $deviceService;

    /**
     * DeviceController constructor.
     * @param DeviceService $deviceService
     */
    public function __construct(DeviceService $deviceService)
    {
        $this->deviceService = $deviceService;
    }

    public function updateStatus(int $deviceId, DeviceStatusRequest $request)
    {
        $userId = Auth::id();

        $updateStatus = $this->deviceService->updateStatus($deviceId, $userId, $request->input('status'));

        if ($updateStatus) {
            return jsonSuccess();
        }
        return jsonServerError(['message' => 'Ошибка БД']);
    }

}