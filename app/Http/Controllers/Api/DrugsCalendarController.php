<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\DrugsCalendarRequest;
use App\Http\Controllers\Controller;
use App\Services\DrugsCalendarService;

class DrugsCalendarController extends Controller
{
    protected $drugsCalendarService;

    /**
     * DrugsCalendarController constructor.
     *
     * @param DrugsCalendarService $drugsCalendarService
     */
    public function __construct(DrugsCalendarService $drugsCalendarService)
    {
        $this->drugsCalendarService = $drugsCalendarService;
    }

    public function getOne(int $calendarId)
    {
        $userId = Auth::id();

        if (!$this->drugsCalendarService->checkCalendarOwner($calendarId, $userId)) {
            return jsonNotFound(['message' => 'Календарь не найден']);
        }

        $calendar = $this->drugsCalendarService->find($calendarId);
        return jsonSuccess($calendar);
    }

    public function getAll()
    {
        $userId = Auth::id();

        $calendars = $this->drugsCalendarService->getAllByUser($userId);
        return jsonSuccess($calendars);
    }

    public function add(DrugsCalendarRequest $request)
    {
        $userId = Auth::id();

        $calendar = $this->drugsCalendarService->add([
            'user_id' => $userId,
            'drug_name' => $request->input('drug_name', '') ?? '',
            'form' => $request->input('form', '') ?? '',
            'date_start' => $request->input('date_start') ?? null,
            'days' => $request->input('days', 0) ?? 0,
            'schedule' => $request->input('schedule', '') ?? '',
            'times' => $request->input('times', '') ?? '',
            'notify' => $request->input('notify', 0) ?? 0,
        ]);
        if ($calendar) {
            return jsonSuccessCreate([
                'id' => $calendar->id
            ]);
        }
        return jsonServerError(['message' => 'Ошибка БД.']);
    }

    public function edit(int $calendarId, DrugsCalendarRequest $request)
    {
        $userId = Auth::id();

        if (!$this->drugsCalendarService->checkCalendarOwner($calendarId, $userId)) {
            return jsonNotFound(['message' => 'Календарь не найден']);
        }

        $calendar = $this->drugsCalendarService->edit($calendarId, [
            'drug_name' => $request->input('drug_name', '') ?? '',
            'form' => $request->input('form', '') ?? '',
            'date_start' => $request->input('date_start') ?? null,
            'days' => $request->input('days', 0) ?? 0,
            'schedule' => $request->input('schedule', '') ?? '',
            'times' => $request->input('times', '') ?? '',
            'notify' => $request->input('notify', 0) ?? 0,
        ]);
        if ($calendar) {
            return jsonSuccess();
        }
        return jsonServerError(['message' => 'Ошибка БД.']);
    }

    public function delete(int $calendarId)
    {
        $userId = Auth::id();

        if (!$this->drugsCalendarService->checkCalendarOwner($calendarId, $userId)) {
            return jsonNotFound(['message' => 'Календарь не найден']);
        }

        if ($this->drugsCalendarService->delete($calendarId)) {
            return jsonSuccess();
        }
        return jsonServerError(['message' => 'Ошибка БД']);
    }

}