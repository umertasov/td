<?php

namespace App\Http\Controllers\Api\Measurements;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Services\EcgService;

class EcgController extends Controller
{
    protected $ecgService;

    /**
     * DeviceController constructor.
     * @param EcgService $ecgService
     */
    public function __construct(EcgService $ecgService)
    {
        $this->ecgService = $ecgService;
    }

    public function getAll()
    {
        $userId = Auth::id();

        $measurements = $this->ecgService->getAllByUser($userId);

        return jsonSuccess($measurements);
    }

    public function add(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'device_id' => 'required|string',
            'date' => 'required|date',
            'heartbeat' => 'required|integer',
            'systolic_pressure' => 'required|integer',
            'diastolic_pressure' => 'required|integer',
            'indications' => 'required|numeric',
            'signal_off' => 'in:0,1',
            'bad_contact' => 'in:0,1',
            'health_index' => 'required|integer',
            'fatigue_index' => 'required|integer',
            'load_index﻿' => 'required|integer',
            'body_index' => 'required|integer',
            'heart_index' => 'required|integer',
        ]);

        $ecg = $this->ecgService->add([
            'user_id' => $userId,
            'device_id' => $request->input('device_id'),
            'date' => strtotime($request->input('date')),
            'heartbeat' => $request->input('heartbeat'),
            'systolic_pressure' => $request->input('systolic_pressure'),
            'diastolic_pressure' => $request->input('diastolic_pressure'),
            'indications' => $request->input('indications'),
            'signal_off' => $request->input('signal_off', 0) ?? 0,
            'bad_contact' => $request->input('bad_contact', 0) ?? 0,
            'health_index' => $request->input('health_index'),
            'fatigue_index' => $request->input('fatigue_index'),
            'load_index﻿' => $request->input('load_index﻿'),
            'body_index' => $request->input('body_index'),
            'heart_index' => $request->input('heart_index'),
        ]);

        return jsonSuccessCreate(['id' => $ecg->id]);
    }
}