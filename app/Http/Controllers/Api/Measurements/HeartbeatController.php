<?php

namespace App\Http\Controllers\Api\Measurements;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Services\HeartbeatService;

class HeartbeatController extends Controller
{
    protected $heartbeatService;

    /**
     * DeviceController constructor.
     * @param HeartbeatService $heartbeatService
     */
    public function __construct(HeartbeatService $heartbeatService)
    {
        $this->heartbeatService = $heartbeatService;
    }

    public function getAll()
    {
        $userId = Auth::id();

        $measurements = $this->heartbeatService->getAllByUser($userId);

        return jsonSuccess($measurements);
    }

    public function add(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'device_id' => 'required|string',
            'date' => 'required|date',
            'value' => 'required|integer',
            'time_values' => 'required|string',
            'day_avg' => 'required|integer',
            'day_max' => 'required|integer',
            'day_min' => 'required|integer',
            'sleep_avg' => 'required|integer',
            'sleep_max' => 'required|integer',
            'sleep_min' => 'required|integer',
            'recent' => 'required|integer',
        ]);

        $heartbeat = $this->heartbeatService->add([
            'user_id' => $userId,
            'device_id' => $request->input('device_id'),
            'date' => strtotime($request->input('date')),
            'value' => $request->input('value'),
            'time_values' => $request->input('time_values'),
            'indications' => $request->input('indications'),
            'day_avg' => $request->input('day_avg'),
            'day_max' => $request->input('day_max'),
            'day_min' => $request->input('day_min'),
            'sleep_avg' => $request->input('sleep_avg'),
            'sleep_max' => $request->input('sleep_max'),
            'sleep_min' => $request->input('sleep_min'),
            'recent' => $request->input('recent'),
        ]);

        return jsonSuccessCreate(['id' => $heartbeat->id]);
    }
}