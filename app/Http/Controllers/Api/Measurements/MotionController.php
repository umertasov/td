<?php

namespace App\Http\Controllers\Api\Measurements;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Services\MotionService;

class MotionController extends Controller
{
    protected $motionService;

    /**
     * DeviceController constructor.
     * @param MotionService $motionService
     */
    public function __construct(MotionService $motionService)
    {
        $this->motionService = $motionService;
    }

    public function getAll()
    {
        $userId = Auth::id();

        $measurements = $this->motionService->getAllByUser($userId);

        return jsonSuccess($measurements);
    }

    public function add(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'device_id' => 'required|string',
            'date' => 'required|date',
            'calorie' => 'required|numeric',
            'distance' => 'required|numeric',
            'step' => 'required|integer',
            'data' => 'required|string',
        ]);

        $motion = $this->motionService->add([
            'user_id' => $userId,
            'device_id' => $request->input('device_id'),
            'date' => strtotime($request->input('date')),
            'calorie' => $request->input('calorie'),
            'distance' => $request->input('distance'),
            'step' => $request->input('step'),
            'data' => $request->input('data'),
        ]);

        return jsonSuccessCreate(['id' => $motion->id]);
    }
}