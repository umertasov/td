<?php

namespace App\Http\Controllers\Api\Measurements;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Services\PpgService;

class PpgController extends Controller
{
    protected $ppgService;

    /**
     * DeviceController constructor.
     * @param PpgService $ppgService
     */
    public function __construct(PpgService $ppgService)
    {
        $this->ppgService = $ppgService;
    }

    public function getAll()
    {
        $userId = Auth::id();

        $measurements = $this->ppgService->getAllByUser($userId);

        return jsonSuccess($measurements);
    }

    public function add(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'device_id' => 'required|string',
            'date' => 'required|date',
            'data' => 'required|numeric',
        ]);

        $ppg = $this->ppgService->add([
            'user_id' => $userId,
            'device_id' => $request->input('device_id'),
            'date' => strtotime($request->input('date')),
            'data' => $request->input('data'),
        ]);

        return jsonSuccessCreate(['id' => $ppg->id]);
    }
}