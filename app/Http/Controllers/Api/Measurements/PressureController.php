<?php

namespace App\Http\Controllers\Api\Measurements;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Services\PressureService;

class PressureController extends Controller
{
    protected $pressureService;

    /**
     * DeviceController constructor.
     * @param PressureService $pressureService
     */
    public function __construct(PressureService $pressureService)
    {
        $this->pressureService = $pressureService;
    }

    public function getAll()
    {
        $userId = Auth::id();

        $measurements = $this->pressureService->getAllByUser($userId);

        return jsonSuccess($measurements);
    }

    public function add(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'device_id' => 'required|string',
            'date' => 'required|date',
            'systolic' => 'required|integer',
            'diastolic' => 'required|integer',
        ]);

        $pressure = $this->pressureService->add([
            'user_id' => $userId,
            'device_id' => $request->input('device_id'),
            'date' => strtotime($request->input('date')),
            'systolic' => $request->input('systolic'),
            'diastolic' => $request->input('diastolic'),
        ]);

        return jsonSuccessCreate(['id' => $pressure->id]);
    }
}