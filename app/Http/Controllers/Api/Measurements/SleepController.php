<?php

namespace App\Http\Controllers\Api\Measurements;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Services\SleepService;

class SleepController extends Controller
{
    protected $sleepService;

    /**
     * DeviceController constructor.
     * @param SleepService $sleepService
     */
    public function __construct(SleepService $sleepService)
    {
        $this->sleepService = $sleepService;
    }

    public function getAll()
    {
        $userId = Auth::id();

        $measurements = $this->sleepService->getAllByUser($userId);

        return jsonSuccess($measurements);
    }

    public function add(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'device_id' => 'required|string',
            'date' => 'required|date',
            'time_total' => 'required|integer',
            'time_deep' => 'required|integer',
            'time_light' => 'required|integer',
            'time_stayup' => 'required|integer',
            'waking_number' => 'required|integer',
            'data' => 'required|string',
            'total' => 'required|integer'
        ]);

        $sleep = $this->sleepService->add([
            'user_id' => $userId,
            'device_id' => $request->input('device_id'),
            'date' => strtotime($request->input('date')),
            'time_total' => $request->input('time_total'),
            'time_deep' => $request->input('time_deep'),
            'time_light' => $request->input('time_light'),
            'time_stayup' => $request->input('time_stayup'),
            'waking_number' => $request->input('waking_number'),
            'data' => $request->input('data'),
            'total' => $request->input('total'),
        ]);

        return jsonSuccessCreate(['id' => $sleep->id]);
    }
}