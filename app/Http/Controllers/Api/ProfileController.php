<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Services\UserService;
use App\Services\BiometricParamService;

class ProfileController extends Controller
{
    protected $userService, $biometricParamService;

    /**
     * ProfileController constructor.
     *
     * @param UserService $userService
     * @param BiometricParamService $biometricParamService
     */
    public function __construct(UserService $userService, BiometricParamService $biometricParamService)
    {
        $this->userService = $userService;
        $this->biometricParamService = $biometricParamService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile()
    {
        $userId = Auth::id();

        $user = $this->userService->find($userId);

        $user['biometric_params'] = $this->biometricParamService->find($userId);

        return jsonSuccess($user);
    }

    public function edit(Request $request)
    {
        $userId = Auth::id();

        $request->validate([
            'first_name' => 'required|string|max:255',
            'city' => 'string|max:255',
            'gender' => 'integer|in:0,1',
            'birth_date' => 'date',
            'height' => 'min:0|max:500',
            'weight' => 'min:0|max:350',
            'hand' => 'integer|in:0,1',
            'goal_steps_day' => 'integer',
            'goal_sleep_day' => 'min:0|max:24',
            'systolic_pressure' => 'integer|min:0|max:255',
            'diastolic_pressure' => 'integer|min:0|max:255',
            'pulse' => 'integer|min:0|max:255'
        ]);

        DB::beginTransaction();
        try {
            $this->userService->edit($userId, [
                'first_name' => $request->input('first_name'),
                'city' => $request->input('city', '') ?? '',
                'gender' => $request->input('gender', 0) ?? 0,
                'birth_date' => $request->input('birth_date') ?? null
            ]);
            $this->biometricParamService->edit($userId, [
                'height' => $request->input('height', 0) ?? 0,
                'weight' => $request->input('weight', 0) ?? 0,
                'hand' => $request->input('hand', 1) ?? 1,
                'goal_steps_day' => $request->input('goal_steps_day', 0) ?? 0,
                'goal_sleep_day' => $request->input('goal_sleep_day', 0) ?? 0,
                'systolic_pressure' => $request->input('systolic_pressure', 0) ?? 0,
                'diastolic_pressure' => $request->input('diastolic_pressure', 0) ?? 0,
                'pulse' => $request->input('pulse', 0) ?? 0,
            ]);

            DB::commit();

            return jsonSuccess();
        } catch (\Exception $e) {
            DB::rollback();
            return jsonServerError();
        }
    }

}
