<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use App\Services\BiometricParamService;
use App\Services\DeviceParamService;

use App\Models\User;

class AuthController extends Controller
{
    use AuthenticatesUsers, RegistersUsers {
        AuthenticatesUsers::guard insteadof RegistersUsers;
    }

    protected $biometricParamService, $deviceParamService;

    protected $redirectTo = '/profile';

    public function __construct(
        BiometricParamService $biometricParamService,
        DeviceParamService $deviceParamService
    )
    {
        $this->biometricParamService = $biometricParamService;
        $this->deviceParamService = $deviceParamService;
    }

    /**
     * Handle an authentication attempt.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function authenticate(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->intended('profile');
        }

        $credentials['phone'] = $credentials['email'];
        unset($credentials['email']);

        if (Auth::attempt($credentials)) {
            return redirect()->intended('profile');
        }

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function registration(RegistrationRequest $request)
    {
        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    protected function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/profile';
    }

    protected function create(array $data)
    {
        $user = User::create([
            'last_name' => $data['last_name'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $this->biometricParamService->edit($user->id, []);
        $this->deviceParamService->edit($user->id, []);

        return $user;
    }
}