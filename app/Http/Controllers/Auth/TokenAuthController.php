<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests\RegistrationRequest;
use Illuminate\Support\Facades\Hash;
use App\Services\UserService;

class TokenAuthController extends Controller
{
    protected $userService, $biometricParamService, $deviceParamService;

    /**
     * ProfileController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return jsonValidationError([
                    'message' => 'Неправильный email или пароль'
                ], 'Авторизация');
            }
        } catch (JWTException $e) {
            return jsonServerError([], 'Авторизация');
        }

        //$token = JWTAuth::getToken();
        $user = JWTAuth::setToken($token)->toUser();

        $avatar = $this->userService->getAvatar($user->id);

        return jsonSuccess(['token' => $token, 'user' => $user, 'avatar' => $avatar]);
    }

    public function registration(RegistrationRequest $request)
    {
        $user = $this->userService->add([
            'last_name' => $request->input('last_name', '') ?? '',
            'first_name' => $request->input('first_name', '') ?? '',
            'middle_name' => $request->input('middle_name', '') ?? '',
            'phone' => $request->input('phone'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        $token = JWTAuth::fromUser($user);

        return jsonSuccess([
            'token' => $token,
            'user' => [
                'id' => $user->id,
                'last_name' => $user->last_name,
                'first_name' => $user->first_name,
                'middle_name' => $user->middle_name,
                'phone' => $user->phone,
                'email' => $user->email,
            ]
        ]);
    }
}