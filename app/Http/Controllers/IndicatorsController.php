<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\UserService;
use App\Services\EcgService;
use App\Services\HeartbeatService;
use App\Services\MotionService;
use App\Services\PpgService;
use App\Services\PressureService;
use App\Services\SleepService;

class IndicatorsController extends Controller
{
    protected $userId, $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;

        $this->middleware(function (Request $request, $next) {
            if (!Auth::check()) {
                return redirect('login');
            }
            $this->userId = Auth::id();

            return $next($request);
        });
    }

    public function indicators()
    {
        $user = $this->userService->getUserInfo($this->userId, [
            'last_name', 'first_name', 'middle_name', 'email', 'email_verified_at',
            'phone', 'gender', 'address', 'city', 'birth_date'
        ]);

        $ecg = app(EcgService::class)->getLast($this->userId);
        $heartbeat = app(HeartbeatService::class)->getLast($this->userId);
        $motions = app(MotionService::class)->getLast($this->userId);
        $ppg = app(PpgService::class)->getLast($this->userId);
        $pressure = app(PressureService::class)->getLast($this->userId);
        $sleep = app(SleepService::class)->getLast($this->userId);

        if (!empty($sleep)) {
            $sleep->hours = 0;
            $sleep->minutes = 0;

            $sleep->hours = floor($sleep->time_total / 60);
            $sleep->minutes = $sleep->time_total - $sleep->hours * 60;
        }

        return view('indicators/indicators', [
            'user' => $user,
            'ecg' => $ecg,
            'heartbeat' => $heartbeat,
            'motions' => $motions,
            'ppg' => $ppg,
            'pressure' => $pressure,
            'sleep' => $sleep
        ]);
    }

    public function heartbeat()
    {
        $user = $this->userService->getUserInfo($this->userId, [
            'last_name', 'first_name', 'middle_name', 'email', 'email_verified_at',
            'phone', 'gender', 'address', 'city', 'birth_date'
        ]);

        $heartbeat = app(HeartbeatService::class)->getAllByUser($this->userId);

        return view('indicators/heartbeat', [
            'user' => $user,
            'heartbeat' => $heartbeat
        ]);
    }

    public function pressure()
    {
        $user = $this->userService->getUserInfo($this->userId, [
            'last_name', 'first_name', 'middle_name', 'email', 'email_verified_at',
            'phone', 'gender', 'address', 'city', 'birth_date'
        ]);

        $pressure = app(PressureService::class)->getAllByUser($this->userId);

        return view('indicators/pressure', [
            'user' => $user,
            'pressure' => $pressure
        ]);
    }

    public function ecg()
    {
        $user = $this->userService->getUserInfo($this->userId, [
            'last_name', 'first_name', 'middle_name', 'email', 'email_verified_at',
            'phone', 'gender', 'address', 'city', 'birth_date'
        ]);

        $ecg = app(EcgService::class)->getAllByUser($this->userId);

        return view('indicators/ecg', [
            'user' => $user,
            'ecg' => $ecg
        ]);
    }

    public function motions()
    {
        $user = $this->userService->getUserInfo($this->userId, [
            'last_name', 'first_name', 'middle_name', 'email', 'email_verified_at',
            'phone', 'gender', 'address', 'city', 'birth_date'
        ]);

        $motions = app(MotionService::class)->getAllByUser($this->userId);

        return view('indicators/motions', [
            'user' => $user,
            'motions' => $motions
        ]);
    }

    public function sleep()
    {
        $user = $this->userService->getUserInfo($this->userId, [
            'last_name', 'first_name', 'middle_name', 'email', 'email_verified_at',
            'phone', 'gender', 'address', 'city', 'birth_date'
        ]);

        $sleep = app(SleepService::class)->getAllByUser($this->userId);

        return view('indicators/sleep', [
            'user' => $user,
            'sleep' => $sleep
        ]);
    }

}