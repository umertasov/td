<?php

namespace App\Http\Controllers;

use Validator;
use Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Mail\ChangePassword;
use Illuminate\Http\Request;
use App\Http\Requests\FileRequest;
use App\Http\Requests\BiometricRequest;
use App\Services\UserService;
use App\Services\BiometricParamService;
use App\Services\DeviceService;
use App\Services\EcgService;
use App\Services\HeartbeatService;
use App\Services\MotionService;
use App\Services\PpgService;
use App\Services\PressureService;
use App\Services\SleepService;

class ProfileController extends Controller
{
    protected $userId, $userService, $biometricParamService, $deviceService;

    /**
     * ProfileController constructor.
     * @param UserService $userService
     * @param BiometricParamService $biometricParamService
     * @param DeviceService $deviceService
     */
    public function __construct(UserService $userService,
                                BiometricParamService $biometricParamService,
                                DeviceService $deviceService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
        $this->biometricParamService = $biometricParamService;
        $this->deviceService = $deviceService;

        $this->middleware(function (Request $request, $next) {
            if (!Auth::check()) {
                return redirect('login');
            }
            $this->userId = Auth::id();

            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function profile(Request $request)
    {
        $user = $this->userService->getUserInfo($this->userId, [
            'last_name', 'first_name', 'middle_name', 'email', 'email_verified_at',
            'phone', 'gender', 'address', 'city', 'birth_date'
        ]);

        if (!empty($user->birth_date)) {
            $user->birth_date = date('d.m.Y', strtotime($user->birth_date));
        }

        $biometric = $this->biometricParamService->getUserParams($this->userId);

        $devices = $this->deviceService->getUserDevices($this->userId);

        $ecg = app(EcgService::class)->getAllByUser($this->userId);
        $heartbeat = app(HeartbeatService::class)->getAllByUser($this->userId);
        $motions = app(MotionService::class)->getAllByUser($this->userId);
        $ppg = app(PpgService::class)->getAllByUser($this->userId);
        $pressure = app(PressureService::class)->getAllByUser($this->userId);
        $sleep = app(SleepService::class)->getAllByUser($this->userId);

        if ($request->has('tab') && !empty($request->input('tab'))) {
            $tab = $request->input('tab');
        } else {
            $tab = session('tab') ?? 'profile';
        }

        return view('profile', [
            'user' => $user,
            'biometric' => $biometric,
            'tab' => $tab,
            'devices' => $devices,
            'ecg' => $ecg,
            'heartbeat' => $heartbeat,
            'motions' => $motions,
            'ppg' => $ppg,
            'pressure' => $pressure,
            'sleep' => $sleep,
        ]);
    }

    public function edit(Request $request)
    {
        $this->userId = Auth::id();

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'middle_name' => 'string|max:255',
            'city' => 'nullable|string|max:255',
            'address' => 'nullable|string|max:255',
            'gender' => 'nullable|integer|in:0,1',
            'birth_date' => 'nullable|date',
        ]);

        if ($validator->fails()) {
            return redirect('profile')
                ->withErrors($validator)
                ->with('status', ['result' => 'error', 'message' => 'Неправильно заполнены поля'])
                ->withInput();
        }

        try {
            $birthDate = $request->input('birth_date') ?? null;
            if (!empty($birthDate)) {
                $birthDate = date('Y-m-d', strtotime($birthDate));
            }

            $this->userService->edit($this->userId, [
                'first_name' => $request->input('first_name', '') ?? '',
                'last_name' => $request->input('last_name', '') ?? '',
                'middle_name' => $request->input('middle_name', '') ?? '',
                //'email' => $request->input('email'),
                //'phone' => $request->input('phone'),
                'city' => $request->input('city', '') ?? '',
                'address' => $request->input('address', '') ?? '',
                'gender' => $request->input('gender', 0) ?? 0,
                'birth_date' => $birthDate
            ]);

            return redirect('profile')->with('status', ['result' => 'success', 'message' => 'Данные успешно сохранены']);
        } catch (\Exception $e) {
            return redirect('profile')->with('status', ['result' => 'error', 'message' => 'Произошла ошибка при сохранении данных']);
        }
    }

    public function uploadAvatar(FileRequest $request)
    {
        $file = $this->userService->uploadAvatar($request->file('file'), $this->userId);

        if (!empty($file)) {
            return redirect('profile')->with('status', ['result' => 'success', 'message' => 'Файл успешно загружен']);
        }
        return redirect('profile')->with('status', ['result' => 'error', 'message' => 'Произошла ошибка при сохранении файла']);
    }

    public function editBiometric(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'height' => 'nullable|numeric|min:0|max:500',
            'weight' => 'nullable|numeric|min:0|max:350',
            'hand' => 'nullable|integer|in:0,1',
            'goal_steps_day' => 'nullable|integer',
            'goal_sleep_day' => 'nullable|min:0|max:24',
            'systolic_pressure' => 'nullable|integer|min:0|max:255',
            'diastolic_pressure' => 'nullable|integer|min:0|max:255',
            'pulse' => 'nullable|integer|min:0|max:255'
        ]);

        if ($validator->fails()) {
            return redirect('profile')
                ->withErrors($validator)
                ->withInput()
                ->with('status', ['result' => 'error', 'message' => 'Неправильно заполнены поля'])
                ->with('tab', 'biometric');
        }

        try {
            $this->biometricParamService->edit($this->userId, [
                'height' => $request->input('height', 0) ?? 0,
                'weight' => $request->input('weight', 0) ?? 0,
                'hand' => $request->input('hand', 1) ?? 1,
                'goal_steps_day' => $request->input('goal_steps_day', 0) ?? 0,
                'goal_sleep_day' => $request->input('goal_sleep_day', 0) ?? 0,
                'systolic_pressure' => $request->input('systolic_pressure', 0) ?? 0,
                'diastolic_pressure' => $request->input('diastolic_pressure', 0) ?? 0,
                'pulse' => $request->input('pulse', 0) ?? 0,
            ]);
            return redirect('profile')->withInput()->with('status', ['result' => 'success', 'message' => 'Данные успешно сохранены'])->with('tab', 'biometric');
        } catch (\Exception $e) {
            return redirect('profile')->withInput()->with('status', ['result' => 'error', 'message' => 'Произошла ошибка при сохранении данных'])->with('tab', 'biometric');
        }
    }

    public function editSecurity(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => "required|email|unique:users,email,{$this->userId}",
            'phone' => "required|string|unique:users,phone,{$this->userId}",
            'password_old' => 'nullable|required_with:password_new|string',
            'password_new' => 'nullable|string',
            'password_confirmation' => 'nullable|required_with:password_new|same:password_new',
        ]);

        if ($validator->fails()) {
            return redirect('profile')
                ->withErrors($validator)
                ->with('tab', 'security')
                ->with('status', ['result' => 'error', 'message' => 'Неправильно заполнены поля']);
        }

        try {
            $updateUser = [
                'email' => $request->input('email'),
                'phone' => $request->input('phone'),
            ];

            if ($request->has('password_old') && !empty($request->input('password_old'))) {
                if (!$this->userService->checkPassword($this->userId, $request->input('password_old'))) {
                    return redirect('profile')
                        ->with('status', ['result' => 'error', 'message' => 'Введен неверный пароль'])
                        ->with('tab', 'security')
                        ->withErrors(['password_old' => ['Неверный пароль']]);
                }

                if ($request->has('password_new') && !empty($request->input('password_new'))) {
                    $updateUser['password'] = Hash::make($request->input('password_new'));
                }
            }

            $updateStatus = $this->userService->edit($this->userId, $updateUser);

            if ($updateStatus && !empty($updateUser['password'])) {
                $user = $this->userService->getUserInfo($this->userId, ['email']);
                Mail::to($user->email)->send(new ChangePassword());
            }

            return redirect('profile')->with('status', ['result' => 'success', 'message' => 'Данные успешно сохранены'])->with('tab', 'security');
        } catch (\Exception $e) {
            return redirect('profile')->with('status', ['result' => 'error', 'message' => 'Произошла ошибка при сохранении данных'])->with('tab', 'security');
        }
    }

}
