<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class TokenAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if (!$user = JWTAuth::parseToken()->authenticate()) {
                return jsonAuthorizationError(['message' => 'token expired']);
            }
        } catch (TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
                $user = JWTAuth::setToken($refreshed)->toUser();
                header('Authorization: Bearer ' . $refreshed);
            } catch (JWTException $e) {
                return jsonAuthorizationError(['message' => 'token expired']);
            }
        } catch (JWTException $e) {
            return jsonAuthorizationError(['message' => 'wrong token']);
        }

        // Don't forget logout!
        Auth::login($user);

        $response = $next($request);

        Auth::logout();

        return $response;
    }
}
