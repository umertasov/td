<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class BiometricRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'height' => 'nullable|min:0|max:500',
            'weight' => 'nullable|min:0|max:350',
            'hand' => 'nullable|integer|in:0,1',
            'goal_steps_day' => 'nullable|integer',
            'goal_sleep_day' => 'nullable|min:0|max:24',
            'systolic_pressure' => 'nullable|integer|min:0|max:255',
            'diastolic_pressure' => 'nullable|integer|min:0|max:255',
            'pulse' => 'nullable|integer|min:0|max:255'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $this->session()->flash('tab', 'biometric');
        $this->session()->flash('status', ['result' => 'error', 'message' => 'Неправильно заполнены поля']);

        parent::failedValidation($validator);
    }
}