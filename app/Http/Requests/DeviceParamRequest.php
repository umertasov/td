<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeviceParamRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'hand_up' => 'nullable|in:0,1',
            'call' => 'nullable|in:0,1',
            'sms' => 'nullable|in:0,1',
            'heartbeat' => 'nullable|in:0,1',
            'theme' => 'nullable|in:0,1',
            'whatsapp' => 'nullable|in:0,1',
            'viber' => 'nullable|in:0,1',
            'vk' => 'nullable|in:0,1',
            'facebook' => 'nullable|in:0,1',
            'twitter' => 'nullable|in:0,1',
            'skype' => 'nullable|in:0,1',
        ];
    }
}