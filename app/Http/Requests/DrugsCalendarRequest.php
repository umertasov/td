<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DrugsCalendarRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'drug_name' => 'required|string',
            'form' => 'required|string',
            'date_start' => 'required|date',
            'days' => 'required|integer|min:0',
            'schedule' => 'string',
            'times' => 'string',
            'notify' => 'in:0,1'
        ];
    }
}