<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BiometricParam extends Model
{
    protected $table = 'biometric_params';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'height', 'weight', 'hand', 'goal_steps_day', 'goal_sleep_day',
        'systolic_pressure', 'diastolic_pressure', 'pulse'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
}
