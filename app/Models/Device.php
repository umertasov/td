<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'devices';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'address'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function drugs_calendar()
    {
        return $this->hasMany(DrugsCalendar::class, 'device_id', 'id');
    }

    public function ecg()
    {
        return $this->hasMany(Ecg::class, 'device_id', 'id');
    }

    public function ppg()
    {
        return $this->hasMany(Ppg::class, 'device_id', 'id');
    }

    public function heartbeat()
    {
        return $this->hasMany(Heartbeat::class, 'device_id', 'id');
    }

    public function motions()
    {
        return $this->hasMany(Motion::class, 'device_id', 'id');
    }

    public function pressure()
    {
        return $this->hasMany(Pressure::class, 'device_id', 'id');
    }

    public function sleep()
    {
        return $this->hasMany(Sleep::class, 'device_id', 'id');
    }

}
