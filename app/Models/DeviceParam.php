<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceParam extends Model
{
    protected $table = 'device_params';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'hand_up', 'call', 'sms', 'heartbeat', 'theme',
        'whatsapp', 'viber', 'vk', 'facebook', 'twitter', 'skype'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
