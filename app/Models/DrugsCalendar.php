<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DrugsCalendar extends Model
{
    protected $table = 'drugs_calendar';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'drug_name', 'form', 'date_start', 'days', 'schedule', 'times', 'notify'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }
}
