<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ecg extends Model
{
    protected $table = 'ecg';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'device_id', 'date', 'heartbeat', 'systolic_pressure', 'diastolic_pressure',
        'indications', 'signal_off', 'bad_contact', 'health_index', 'fatigue_index',
        'load_index﻿', 'body_index', 'heart_index'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }
}
