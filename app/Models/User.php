<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Notifications\VerificationEmailNotification;

class User extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'last_name', 'middle_name',
        'email', 'phone', 'gender', 'address', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'created_at', 'updated_at'
    ];

    public function avatar()
    {
        return $this->hasOne(File::class, 'id', 'file_id');
    }

    public function devices()
    {
        return $this->hasMany(Device::class, 'user_id', 'id');
    }

    public function drugs_calendar()
    {
        return $this->hasMany(DrugsCalendar::class, 'user_id', 'id');
    }

    public function ecg()
    {
        return $this->hasMany(Ecg::class, 'user_id', 'id');
    }

    public function ppg()
    {
        return $this->hasMany(Ppg::class, 'user_id', 'id');
    }

    public function heartbeat()
    {
        return $this->hasMany(Heartbeat::class, 'user_id', 'id');
    }

    public function motions()
    {
        return $this->hasMany(Motion::class, 'user_id', 'id');
    }

    public function pressure()
    {
        return $this->hasMany(Pressure::class, 'user_id', 'id');
    }

    public function sleep()
    {
        return $this->hasMany(Sleep::class, 'user_id', 'id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerificationEmailNotification());
    }
}
