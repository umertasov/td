<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use App\Mail\ResetPassword as Mailable;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param $notifiable
     * @return $this
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Сброс пароля на TD24')
            ->line('Вы получили данное письмо, потому что Вы запросили сброс пароля для учетной записи')
            ->action('Сбросить пароль', config('app.url') . '/password/reset/' . $this->token)
            ->line('Если Вы не делали данный запрос, просто проигнорируйте это письмо.');
    }
}
