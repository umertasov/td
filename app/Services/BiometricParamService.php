<?php

namespace App\Services;

use App\Models\BiometricParam;

class BiometricParamService extends Service
{
    public function __construct(BiometricParam $biometricParam)
    {
        $this->model = $biometricParam;
    }

    public function find(int $userId)
    {
        return $this->model->where('user_id', $userId)->first();
    }

    public function edit(int $userId, array $values)
    {
        return $this->model->updateOrCreate(
            ['user_id' => $userId],
            $values
        );
    }

    public function getUserParams(int $userId)
    {
        return $this->model->where('user_id', $userId)->first();
    }

}