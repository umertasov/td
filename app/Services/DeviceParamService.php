<?php

namespace App\Services;

use App\Models\DeviceParam;

class DeviceParamService extends Service
{
    public function __construct(DeviceParam $deviceParam)
    {
        $this->model = $deviceParam;
    }

    public function add(array $values)
    {
        return null;
    }

    public function edit(int $userId, array $values)
    {
        return $this->model->updateOrCreate([
            'user_id' => $userId
        ], $values);
    }

    public function getParams(int $userId)
    {
        return $this->model->where('user_id', $userId)->first();
    }

}