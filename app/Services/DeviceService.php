<?php

namespace App\Services;

use App\Models\Device;

class DeviceService extends Service
{
    protected $deviceParamService;

    public function __construct(Device $device, DeviceParamService $deviceParamService)
    {
        $this->model = $device;
        $this->deviceParamService = $deviceParamService;
    }

    public function getUserDevices(int $userId)
    {
        return $this->model->where('user_id', $userId)->get();
    }

    public function add(array $values)
    {
        if (!isset($values['name']) || empty($values['name'])) {
            return null;
        }

        $values['id'] = $values['name'];

        unset($values['name']);

        $device = $this->model->updateOrCreate(
            ['id' => $values['id']],
            $values
        );

        $this->deviceParamService->edit($device->user_id, [
            'user_id' => $device->user_id
        ]);

        return $device;
    }

    public function updateStatus(int $deviceId, int $userId, int $status)
    {
        return $this->model->where('id', $deviceId)->where('user_id', $userId)->update([
            'status' => $status,
            'date_status' => time(),
        ]);
    }

}