<?php

namespace App\Services;

use App\Models\DrugsCalendar;

class DrugsCalendarService extends MeasurementService
{
    public function __construct(DrugsCalendar $drugsCalendar)
    {
        $this->model = $drugsCalendar;
    }

    public function checkCalendarOwner(int $calendarId, int $userId)
    {
        $calendar = $this->model->where('id', $calendarId)->where('user_id', $userId)
            ->select(['id', 'user_id'])->first();
        if ($calendar) {
            return true;
        }
        return false;
    }

}