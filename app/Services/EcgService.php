<?php

namespace App\Services;

use App\Models\Ecg;

class EcgService extends MeasurementService
{
    public function __construct(Ecg $ecg)
    {
        $this->model = $ecg;
    }

}