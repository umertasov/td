<?php

namespace App\Services;

use App\Models\Heartbeat;

class HeartbeatService extends MeasurementService
{
    public function __construct(Heartbeat $heartbeat)
    {
        $this->model = $heartbeat;
    }

}