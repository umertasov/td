<?php

namespace App\Services;

class MeasurementService extends Service
{
    public function getAllByUser(int $userId)
    {
        return $this->model->where('user_id', $userId)->get();
    }

    public function getLast(int $userId)
    {
        return $this->model->where('user_id', $userId)->orderBy('date', 'desc')->first();
    }

}