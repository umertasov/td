<?php

namespace App\Services;

use App\Models\Motion;

class MotionService extends MeasurementService
{
    public function __construct(Motion $motion)
    {
        $this->model = $motion;
    }

}