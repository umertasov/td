<?php

namespace App\Services;

use App\Models\Ppg;

class PpgService extends MeasurementService
{
    public function __construct(Ppg $ppg)
    {
        $this->model = $ppg;
    }

}