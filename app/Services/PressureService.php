<?php

namespace App\Services;

use App\Models\Pressure;

class PressureService extends MeasurementService
{
    public function __construct(Pressure $pressure)
    {
        $this->model = $pressure;
    }

}