<?php

namespace App\Services;

use App\Repositories\RoleUserRepository;

class RoleUserService extends Service
{
    public function __construct(RoleUserRepository $roleUserRepository)
    {
        $this->repository = $roleUserRepository;
    }
}