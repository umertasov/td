<?php

namespace App\Services;

abstract class Service
{
    protected $model;

    public function getList()
    {
        return $this->model->get();
    }

    public function find(int $id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function add(array $values)
    {
        return $this->model->create($values);
    }

    public function edit(int $id, array $values)
    {
        return $this->model->where('id', $id)->update($values);
    }

    public function delete(int $id)
    {
        return $this->model->where('id', $id)->delete();
    }
}