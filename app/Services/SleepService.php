<?php

namespace App\Services;

use App\Models\Sleep;

class SleepService extends MeasurementService
{
    public function __construct(Sleep $sleep)
    {
        $this->model = $sleep;
    }

}