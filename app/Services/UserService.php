<?php

namespace App\Services;

use App\Traits\FileTrait;
use Illuminate\Support\Facades\Hash;
use DB;

use App\Models\User;

class UserService extends Service
{
    use FileTrait;

    protected $biometricParamService, $deviceParamService;

    public function __construct(
        User $user,
        BiometricParamService $biometricParamService,
        DeviceParamService $deviceParamService
    )
    {
        $this->model = $user;
        $this->biometricParamService = $biometricParamService;
        $this->deviceParamService = $deviceParamService;
    }

    /**
     * Create new user
     *
     * @param array $values
     * @return bool|int
     */
    public function add(array $values)
    {
        try {
            $user = $this->model->create($values);

            $this->biometricParamService->edit($user->id, []);
            $this->deviceParamService->edit($user->id, []);

            return $user;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getUserInfo(int $userId, array $fields = [])
    {
        $user = $this->model->with(['avatar' => function ($q) {
            $q->select(['id', 'path', 'thumb']);
        }])->where('id', $userId)->select(array_merge([
            'id', 'file_id'
        ], $fields))->first();

        return $user;
    }

    public function checkPassword(int $userId, string $password)
    {
        $user = $this->model->where('id', $userId)->select(['id', 'password'])->first();
        if (!empty($user) && Hash::check($password, $user->password)) {
            return true;
        }
        return false;
    }

    public function getAvatar(int $userId)
    {
        $user = $this->model->with(['avatar' => function ($q) {
            $q->select(['id', 'path', 'thumb']);
        }])
            ->where('id', $userId)
            ->select(['id', 'file_id'])->first();

        return $user->avatar;
    }

    /**
     * @param null $file
     * @param int $userId
     * @return \App\Entities\FileEntity|bool|null
     * @codeCoverageIgnore
     */
    public function uploadAvatar($file = null, int $userId)
    {
        $extension = $file->guessExtension();
        if (!\in_array($extension, ['png', 'jpg', 'jpeg'])) {
            return false;
        }

        DB::beginTransaction();
        try {
            $file = $this->uploadFile($file, 'avatars', true);
            if (!empty($file)) {
                $userInfo = $this->model->where('id', $userId)->with(['avatar' => function ($q) {
                    $q->select(['id', 'path', 'thumb']);
                }])->select(['id', 'file_id'])->first();
                if ($this->model->where('id', $userId)->update(['file_id' => $file->id])) {
                    if (!empty($userInfo) && !empty($userInfo->file_id)) {
                        $this->deleteFile($userInfo->file_id);
                        if (!empty($userInfo->avatar->path)) {
                            unlink(base_path($userInfo->avatar->path));
                        }
                        if (!empty($userInfo->avatar->thumb)) {
                            unlink(base_path($userInfo->avatar->thumb));
                        }
                    }
                    DB::commit();
                    return $file;
                }
            }
            DB::rollback();
            return false;
        } catch (\Exception $e) {
            DB::rollback();
            return false;
        }
    }

    /**
     * Check user role
     *
     * @param int $userId
     * @param string $roleName
     * @return bool
     */
    public function hasRole(int $userId, string $roleName) : bool
    {
        return $this->model->where('id', $userId)->hasRole($roleName);
    }
}