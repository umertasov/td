<?php

namespace App\Traits;

use App\Entities\FileEntity;
use Intervention\Image\Facades\Image;

use App\Models\File;

trait FileTrait
{
    public function uploadFile($file = null, string $directory = '', bool $thumb = false)
    {
        $extension = $file->guessExtension();
        $mimetype = $file->getMimeType();
        $name = $file->getClientOriginalName();

        $pos = strrpos($name, '.');
        $ext = substr($name, $pos);

        $newFilename = md5(uniqid() . $name) . $ext;

        $one = $two = $three = '';
        sscanf($newFilename, '%2s%2s%s', $one, $two, $three);

        $tempPath = mb_strtolower("../storage/app/public/temp/{$directory}/{$one}/{$two}/{$three}");
        $newPath = mb_strtolower("../storage/app/public/{$directory}/{$one}/{$two}/{$three}");
        $thumbPath = mb_strtolower("../storage/app/public/thumb_{$directory}/{$one}/{$two}/{$three}");

        $filePaste = [
            'path' => mb_strtolower("storage/app/public/{$directory}/{$one}/{$two}/{$three}"),
            'thumb' => '',
            'filename' => $three,
            'mimetype' => $mimetype
        ];

        if (\in_array($extension, ['png', 'jpg', 'jpeg'])) {
            $this->createDirectories([
                "../storage/app/public/{$directory}",
                "../storage/app/public/{$directory}/{$one}",
                "../storage/app/public/{$directory}/{$one}/{$two}"
            ]);

            if ($file->move("../storage/app/public/temp/{$directory}/{$one}/{$two}", $three)) {
                Image::make($tempPath)->fit(500)->save($newPath);
                if ($thumb) {
                    $this->createDirectories([
                        "../storage/app/public/thumb_{$directory}",
                        "../storage/app/public/thumb_{$directory}/{$one}",
                        "../storage/app/public/thumb_{$directory}/{$one}/{$two}"
                    ]);

                    Image::make($tempPath)->fit(150)->save($thumbPath);
                    $filePaste['thumb'] = mb_strtolower("storage/app/public/thumb_{$directory}/{$one}/{$two}/{$three}");
                }
                unlink($tempPath);
                $fileCreated = File::create($filePaste);
                if ($fileCreated) {
                    return new FileEntity($fileCreated->id, $filePaste['path'], $filePaste['thumb'], $filePaste['filename'], $filePaste['mimetype']);
                }
            }
        } else {
            if ($file->move("../storage/app/public/{$directory}/{$one}/{$two}", $three)) {
                $fileCreated = File::create($filePaste);
                if ($fileCreated) {
                    return new FileEntity($fileCreated->id, $filePaste['path'], $filePaste['thumb'], $filePaste['filename'], $filePaste['mimetype']);
                }
            }
        }
        return false;
    }

    public function deleteFile($fileId)
    {
        if (!empty($fileId)) {
            return File::where('id', $fileId)->delete();
        }
        return false;
    }

    private function createDirectories(array $dirs) : void
    {
        foreach ($dirs ?? [] as $dir) {
            try {
                mkdir($dir);
            } catch (\Exception $e) {}
        }
    }
}