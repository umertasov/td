<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNamesToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->default('')->after('name');
            $table->string('last_name')->default('')->after('first_name');
            $table->string('middle_name')->default('')->after('last_name');
            $table->boolean('gender')->nullable()->after('email')->comment('0 - мужской; 1 - женский;');
            $table->string('address')->default('')->after('gender');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['first_name', 'last_name', 'middle_name', 'gender', 'address']);
        });
    }
}
