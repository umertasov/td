<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiometricParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biometric_params', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->unique()->index('user_id');
            $table->decimal('height', 5, 2)->default(0);
            $table->decimal('weight', 6, 3)->default(0);
            $table->boolean('hand')->default(1)->comment('0 - левая, 1 - правая;');
            $table->integer('goal_steps_day')->unsigned()->default(0);
            $table->integer('goal_steps_hour')->unsigned()->default(0);
            $table->unsignedTinyInteger('systolic_pressure')->default(0);
            $table->unsignedTinyInteger('diastolic_pressure')->default(0);
            $table->unsignedTinyInteger('pulse')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biometric_params');
    }
}
