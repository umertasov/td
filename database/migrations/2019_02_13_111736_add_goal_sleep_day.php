<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoalSleepDay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('biometric_params', function (Blueprint $table) {
            $table->dropColumn(['goal_steps_hour']);
            $table->decimal('goal_sleep_day', 4, 2)->default(0)->after('goal_steps_day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('biometric_params', function (Blueprint $table) {
            $table->dropColumn(['goal_sleep_day']);
            $table->integer('goal_steps_hour')->unsigned()->default(0)->after('goal_steps_day');
        });
    }
}
