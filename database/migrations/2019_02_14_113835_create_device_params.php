<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_params', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('device_params_user_id');
            $table->string('device_id')->index('device_params_device_id');
            $table->boolean('hand_up');
            $table->boolean('call');
            $table->boolean('sms');
            $table->boolean('heartbeat');
            $table->boolean('theme');
            $table->boolean('whatsapp');
            $table->boolean('viber');
            $table->boolean('vk');
            $table->boolean('facebook');
            $table->boolean('twitter');
            $table->boolean('skype');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_params');
    }
}
