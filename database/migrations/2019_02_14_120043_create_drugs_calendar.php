<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrugsCalendar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drugs_calendar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('drugs_calendar_user_id');
            $table->string('device_id')->nullable()->index('drugs_calendar_device_id');
            $table->string('drug_name')->default('');
            $table->string('form')->default('');
            $table->date('date_start')->nullable();
            $table->integer('days')->default(0);
            $table->string('schedule')->default('');
            $table->string('times')->default('');
            $table->boolean('notify')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drugs_calendar');
    }
}
