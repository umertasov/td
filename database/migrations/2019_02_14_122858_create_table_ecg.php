<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEcg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ecg', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('ecg_user_id');
            $table->string('device_id')->nullable()->index('ecg_device_id');
            $table->integer('date')->nullable();
            $table->integer('heartbeat')->unsigned()->default(0);
            $table->unsignedTinyInteger('systolic_pressure')->default(0);
            $table->unsignedTinyInteger('diastolic_pressure')->default(0);
            $table->double('indications')->default(0);
            $table->boolean('signal_off')->default(0);
            $table->boolean('bad_contact')->default(0);
            $table->integer('health_index')->default(0);
            $table->integer('fatigue_index')->default(0);
            $table->integer('load_index﻿')->default(0);
            $table->integer('body_index')->default(0);
            $table->integer('heart_index')->nullable();
            $table->double('blood_pressure')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ecg');
    }
}
