<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHeartbeat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heartbeat', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('heartbeat_user_id');
            $table->string('device_id')->nullable()->index('heartbeat_device_id');
            $table->integer('date')->nullable();
            $table->integer('value')->default('0');
            $table->string('time_values')->default('');
            $table->integer('day_avg')->default('0');
            $table->integer('day_max')->default('0');
            $table->integer('day_min')->default('0');
            $table->integer('sleep_avg')->default('0');
            $table->integer('sleep_max')->default('0');
            $table->integer('sleep_min')->default('0');
            $table->integer('recent')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heartbeat');
    }
}
