<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMotions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('motions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('motions_user_id');
            $table->string('device_id')->nullable()->index('motions_device_id');
            $table->integer('date')->nullable();
            $table->float('calorie')->default(0);
            $table->float('distance')->default(0);
            $table->integer('step')->default(0);
            $table->string('data')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('motions');
    }
}
