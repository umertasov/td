<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePressure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pressure', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('pressure_user_id');
            $table->string('device_id')->nullable()->index('pressure_device_id');
            $table->integer('date')->nullable();
            $table->unsignedTinyInteger('systolic')->default(0);
            $table->unsignedTinyInteger('diastolic')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pressure');
    }
}
