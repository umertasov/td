<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSleep extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sleep', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('sleep_user_id');
            $table->string('device_id')->nullable()->index('sleep_device_id');
            $table->integer('date')->nullable();
            $table->integer('time_total')->default(0);
            $table->integer('time_deep')->default(0);
            $table->integer('time_light')->default(0);
            $table->integer('time_stayup')->default(0);
            $table->integer('waking_number')->default(0);
            $table->string('data')->default('');
            $table->integer('total')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sleep');
    }
}
