<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysFromDevices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_params', function (Blueprint $table) {
            $table->foreign('device_id', 'device_params_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
        Schema::table('drugs_calendar', function (Blueprint $table) {
            $table->foreign('device_id', 'drugs_calendar_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('SET NULL');
        });
        Schema::table('ecg', function (Blueprint $table) {
            $table->foreign('device_id', 'ecg_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('SET NULL');
        });
        Schema::table('heartbeat', function (Blueprint $table) {
            $table->foreign('device_id', 'heartbeat_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('SET NULL');
        });
        Schema::table('motions', function (Blueprint $table) {
            $table->foreign('device_id', 'motions_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('SET NULL');
        });
        Schema::table('pressure', function (Blueprint $table) {
            $table->foreign('device_id', 'pressure_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('SET NULL');
        });
        Schema::table('sleep', function (Blueprint $table) {
            $table->foreign('device_id', 'sleep_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_params', function (Blueprint $table) {
            $table->dropForeign('device_params_device_id');
        });
        Schema::table('drugs_calendar', function (Blueprint $table) {
            $table->dropForeign('drugs_calendar_device_id');
        });
        Schema::table('ecg', function (Blueprint $table) {
            $table->dropForeign('ecg_device_id');
        });
        Schema::table('heartbeat', function (Blueprint $table) {
            $table->dropForeign('heartbeat_device_id');
        });
        Schema::table('motions', function (Blueprint $table) {
            $table->dropForeign('motions_device_id');
        });
        Schema::table('pressure', function (Blueprint $table) {
            $table->dropForeign('pressure_device_id');
        });
        Schema::table('sleep', function (Blueprint $table) {
            $table->dropForeign('sleep_device_id');
        });
    }
}
