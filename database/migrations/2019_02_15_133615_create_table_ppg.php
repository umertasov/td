<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePpg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ppg', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index('ppg_user_id');
            $table->string('device_id')->nullable()->index('ppg_device_id');
            $table->integer('date')->nullable();
            $table->double('data')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppg');
    }
}
