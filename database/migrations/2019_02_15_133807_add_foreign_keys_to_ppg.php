<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToPpg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ppg', function (Blueprint $table) {
            $table->foreign('user_id', 'ppg_user_id')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
            $table->foreign('device_id', 'ppg_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ppg', function (Blueprint $table) {
            $table->dropForeign('ppg_user_id');
            $table->dropForeign('ppg_device_id');
        });
    }
}
