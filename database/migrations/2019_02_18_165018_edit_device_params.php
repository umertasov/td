<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditDeviceParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_params', function (Blueprint $table) {
            $table->boolean('hand_up')->default(0)->change();
            $table->boolean('call')->default(0)->change();
            $table->boolean('sms')->default(0)->change();
            $table->boolean('heartbeat')->default(0)->change();
            $table->boolean('theme')->default(0)->change();
            $table->boolean('whatsapp')->default(0)->change();
            $table->boolean('viber')->default(0)->change();
            $table->boolean('vk')->default(0)->change();
            $table->boolean('facebook')->default(0)->change();
            $table->boolean('twitter')->default(0)->change();
            $table->boolean('skype')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_params', function (Blueprint $table) {
            $table->boolean('hand_up')->change();
            $table->boolean('call')->change();
            $table->boolean('sms')->change();
            $table->boolean('heartbeat')->change();
            $table->boolean('theme')->change();
            $table->boolean('whatsapp')->change();
            $table->boolean('viber')->change();
            $table->boolean('vk')->change();
            $table->boolean('facebook')->change();
            $table->boolean('twitter')->change();
            $table->boolean('skype')->change();
        });
    }
}
