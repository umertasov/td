<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDeviceIdFromDeviceParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('device_params', function (Blueprint $table) {
            $table->dropForeign('device_params_device_id');
            $table->dropColumn('device_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_params', function (Blueprint $table) {
            $table->string('device_id')->index('device_params_device_id');
            $table->foreign('device_id', 'device_params_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('CASCADE');
        });
    }
}
