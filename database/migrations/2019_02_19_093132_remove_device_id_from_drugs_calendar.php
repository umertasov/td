<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDeviceIdFromDrugsCalendar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drugs_calendar', function (Blueprint $table) {
            $table->dropForeign('drugs_calendar_device_id');
            $table->dropColumn('device_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drugs_calendar', function (Blueprint $table) {
            $table->string('device_id')->nullable()->index('drugs_calendar_device_id');
            $table->foreign('device_id', 'drugs_calendar_device_id')->references('id')->on('devices')->onUpdate('RESTRICT')->onDelete('SET NULL');
        });
    }
}
