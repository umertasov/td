$(document).ready(function() {
  pluginsInit();
})

function pluginsInit() {
  $("#birthday").datepicker({
    format: 'dd.mm.yyyy',
    language: "ru",
    locale: "ru"
  });

  $('#gender').select2({
    placeholder: 'Выберите свой пол',
    minimumResultsForSearch: Infinity,
    width: 'resolve'
  });
  
  $('#hand').select2({
    placeholder: 'Ваша основная рука',
    minimumResultsForSearch: Infinity,
    width: 'resolve'
  });
}