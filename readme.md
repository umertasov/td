# ТД24

## Системные требования

* PHP 7.2

Расширения PHP:

* xmlwriter
* pgsql
* pdo_pgsql


## Установка

Для разворачивания проекта на локальной машине понадобятся:

* [Git](https://git-scm.com/downloads)
* [Composer](https://getcomposer.org/download/)
* [Docker Compose](https://docs.docker.com/compose/)

Для скачивания репозитория следует воспользоваться командой:

```bash
git clone https://github.com/neurohotep/td24.git
```

Установить необходимые зависимости:

```bash
composer update
```

Установить Laradock:

```bash
git clone https://github.com/laradock/laradock.git
```

После этого перейти в директорию `/laradock` и выполнить команду:

```bash
cp env-example .env
```

Отредактировать следующие переменные в файле .env:

```bash
APACHE_DOCUMENT_ROOT=/var/www/public/

POSTGRES_DB=td24
POSTGRES_USER=root
POSTGRES_PASSWORD=secret
POSTGRES_PORT=5432
POSTGRES_ENTRYPOINT_INITDB=./postgres/docker-entrypoint-initdb.d
```

Отредактировать параметр DocumentRoot в файле apache2/sites/default.apache.conf:

```bash
DocumentRoot /var/www/public/
```

Запустить контейнеры:

```bash
docker-compose up -d apache2 postgres maildev
```

Далее следует перейти в Docker-контейнер командой:

```bash
docker-compose exec workspace bash
```

Создать файл .env, создав копию файла .env.example.

В файле .env настроить переменные окружения:

```bash
DB_CONNECTION=pgsql
DB_HOST=postgres
DB_PORT=5432
DB_DATABASE=td24
DB_USERNAME=root
DB_PASSWORD=secret

MAIL_DRIVER=smtp
MAIL_HOST=maildev
MAIL_PORT=25
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```

Выполнить команды:

```bash
php artisan key:generate
php artisan optimize
```

Для отображения загруженных файлов создать символическую ссылку:

```bash
cd public
ln -s ../storage storage
```

Работать.