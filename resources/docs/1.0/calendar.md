# Календарь приема лекарств

* [Список курсов](#get)
* [Получение курса](#get_one)
* [Добавление курса](#add)
* [Редактирование курса](#edit)
* [Удаление курса](#delete)

---

<a name="get">
## Список курсов

**URL**: `/api/user/calendar`

**Метод**: GET

**Ответ**: 

```javascript
200 OK: {
    'status': 'success', 
    'data': {
        // список курсов
        ...
    }
}
```

---

<a name="get_one">
## Получение курса

**URL**: `/api/user/calendar/{calendar_id}`

**Метод**: GET

**Ответ**: 

```javascript
200 OK: {
    'status': 'success', 
    'data': {
        // ...
    }
}
```

---

<a name="add">
## Добавление курса

**URL**: `/api/user/calendar`

**Метод**: POST

**Параметры**:

| | |
| ------ | ------------------------------ |
| **`drug_name`**       |  Название лекарства |
| **`form`**   | Форма выпуска |
| **`date_start`**   | Дата начала (YYYY-MM-DD) |
| **`days`**   | Кол-во дней приема |
| `schedule`   | Расписание приема |
| `times`   | Времена приема лекарств |
| `notify`   | Включить уведомления |

**Ответ**: 

```javascript
201 OK: {
    'status': 'success',
    'id': 'id записи'
}
```

---

<a name="edit">
## Редактирование курса

**URL**: `/api/user/calendar/{calendar_id}`

**Метод**: POST

**Параметры**:

| | |
| ------ | ------------------------------ |
| **`drug_name`**       |  Название лекарства |
| **`form`**   | Форма выпуска |
| **`date_start`**   | Дата начала |
| **`days`**   | Кол-во дней приема |
| `schedule`   | Расписание приема |
| `times`   | Времена приема лекарств |
| `notify`   | Включить уведомления |

**Ответ**: 

```javascript
200 OK: {
    'status': 'success'
}
```

---

<a name="delete">
## Удаление курса

**URL**: `/api/user/calendar/{calendar_id}`

**Метод**:  DELETE

**Ответ**: 

```javascript
200 OK: {
    'status': 'success'
}
```