# ЭКГ

* [Список замеров](#get)
* [Добавление замера](#add)

---

<a name="get">
## Список замеров

**URL**: `/api/user/measurements/ecg`

**Метод**: GET

**Ответ**: 

```javascript
200 OK: {
    'status': 'success', 
    'data': {
        // список замеров
        ...
    }
}
```

---

<a name="add">
## Добавление замера

**URL**: `/api/user/measurements/ecg`

**Метод**: POST

**Параметры**:

| | |
| ------ | ------------------------------ |
| **`device_id`**       |  ID устройства |
| **`date`**   | Дата замера |
| **`heartbeat`**   | Серцебиение |
| **`systolic_pressure`**   | Систолическое давление |
| **`diastolic_pressure`**   | Дестолическое давление |
| **`indications`**   | Значения ЭКГ |
| **`signal_off`**   | Признак отсутствия сигнала |
| **`bad_contact`**   | Плохой контакт с устройством |
| **`health_index`**   | Общий индекс здоровья |
| **`fatigue_index`**   | Индекс истощения |
| **`load_index﻿`**   | Индекс уровня стресса |
| **`body_index`**   | Индекс энергии |
| **`heart_index`**   | Индекс работы сердца |

**Ответ**: 

```javascript
201 OK: {
    'status': 'success',
    'id': 'id записи'
}
```