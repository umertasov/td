# PPG

* [Список замеров](#get)
* [Добавление замера](#add)

---

<a name="get">
## Список замеров

**URL**: `/api/user/measurements/ppg`

**Метод**: GET

**Ответ**: 

```javascript
200 OK: {
    'status': 'success', 
    'data': {
        // список замеров
        ...
    }
}
```

---

<a name="add">
## Добавление замера

**URL**: `/api/user/measurements/ppg`

**Метод**: POST

**Параметры**:

| | |
| ------ | ------------------------------ |
| **`device_id`**       |  ID устройства |
| **`date`**   | Дата замера |
| **`data`**   | Данные по PPG |

**Ответ**: 

```javascript
201 OK: {
    'status': 'success',
    'id': 'id записи'
}
```