@extends('layouts.auth')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-7 pl-md-0">
            <div class="auth-wrap"><a class="auth-logo" href="#"></a>
                <form class="auth-form" id="login" method="POST" action="/login">
                    @csrf
                    <p class="auth-signin">Авторизация</p>
                    <div class="auth-input--wrap">
                        <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} auth-input" name="email" value="{{ old('email') }}" placeholder="Адрес электронной почты" autocomplete="off" required autofocus>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="auth-input--wrap">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} auth-input" name="password" placeholder="Пароль" required>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="auth-forget--wrap text-right"><a class="auth-forget" href="/password/reset">Забыли пароль?</a></div>
                    <button type="submit" class="auth-submit">
                        {{ __('Войти') }}
                    </button>
                    <p class="auth-jump text-center"><span>Нет аккаунта? </span><a href="javascript:void(0);" data-jump="register">Зарегистрируйтесь</a><span>!</span></p>
                </form>
                <form class="auth-form" id="register" method="post" action="{{ route('register') }}" data-show="hidden">
                    @csrf
                    <p class="auth-signin">Регистрация</p>
                    <div class="auth-input--wrap">
                        <input id="last-name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }} auth-input" name="last_name" value="{{ old('last_name') }}" placeholder="Фамилия" autocomplete="off" required autofocus>

                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('last_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="auth-input--wrap">
                        <input id="first-name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }} auth-input" name="first_name" value="{{ old('first_name') }}" placeholder="Имя" autocomplete="off" required>

                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('first_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="auth-input--wrap">
                        <input id="middle-name" type="text" class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }} auth-input" name="middle_name" value="{{ old('middle_name') }}" placeholder="Отчество" autocomplete="off" required>

                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('middle_name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="auth-input--wrap">
                        <input id="reg-email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} auth-input" name="email" value="{{ old('email') }}" placeholder="Электронная почта" autocomplete="off" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="auth-input--wrap">
                        <input id="reg-phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }} auth-input" name="phone" value="{{ old('phone') }}" placeholder="Телефон" autocomplete="off" required>

                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="auth-input--wrap">
                        <input id="reg-password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} auth-input" placeholder="Придумайте пароль" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="auth-input--wrap">
                        <input id="reg-password-confirm" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} auth-input" placeholder="Подтверждение пароля" name="password_confirmation" required>
                    </div>
                    <button class="auth-submit" type="submit">Зарегистрироваться</button>
                    <p class="auth-jump text-center"><span>Зарегистрированы? </span><a href="javascript:void(0);" data-jump="login">Выполните вход</a><span>!</span></p>
                </form>
            </div>
        </div>
        <div class="col-md-5 pr-md-0">
            <div class="auth-side"></div>
        </div>
    </div>
</div>
@endsection
