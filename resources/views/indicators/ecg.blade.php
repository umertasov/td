@extends('layouts.app')

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">ЭКГ</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-12">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Имя устройства</th>
                                    <th>Дата замера</th>
                                    <th>Серцебиение</th>
                                    <th>Систолическое давление</th>
                                    <th>Дестолическое давление</th>
                                    <th>Значения ЭКГ</th>
                                    <th>Признак отсутствия сигнала</th>
                                    <th>Плохой контакт с устройством</th>
                                    <th>Общий индекс здоровья</th>
                                    <th>Индекс истощения</th>
                                    <th>Индекс уровня стресса</th>
                                    <th>Индекс энергии</th>
                                    <th>Индекс работы сердца</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($ecg)): ?>
                                <?php foreach ($ecg as $ecg_item): ?>
                                <tr>
                                    <th scope="row"><?php echo $ecg_item->id; ?></th>
                                    <td><?php echo $ecg_item->device_id; ?></td>
                                    <td><?php echo date('H:i:s d.m.Y', $ecg_item->date); ?></td>
                                    <td><?php echo $ecg_item->heartbeat; ?></td>
                                    <td><?php echo $ecg_item->systolic_pressure; ?></td>
                                    <td><?php echo $ecg_item->diastolic_pressure; ?></td>
                                    <td><?php echo $ecg_item->indications; ?></td>
                                    <td><?php echo $ecg_item->signal_off; ?></td>
                                    <td><?php echo $ecg_item->bad_contact; ?></td>
                                    <td><?php echo $ecg_item->health_index; ?></td>
                                    <td><?php echo $ecg_item->fatigue_index; ?></td>
                                    <td><?php echo $ecg_item->load_index﻿; ?></td>
                                    <td><?php echo $ecg_item->body_index; ?></td>
                                    <td><?php echo $ecg_item->heart_index; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush