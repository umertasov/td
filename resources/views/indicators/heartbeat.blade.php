@extends('layouts.app')

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Сердцебиение</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-12">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Имя устройства</th>
                                    <th>Дата замера</th>
                                    <th>Кол-во ударов в минуту</th>
                                    <th>Сердечный ритм в час</th>
                                    <th>Среднее за день</th>
                                    <th>Максимальное за день</th>
                                    <th>Минимальное за день</th>
                                    <th>Среднее значение продолжительности сна</th>
                                    <th>Максимальное значение продолжительности длины сна</th>
                                    <th>Минимальное значение продолжительности длины сна</th>
                                    <th>Последнее значение измеренного сердечного ритма</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($heartbeat)): ?>
                                <?php foreach ($heartbeat as $heartbeat_item): ?>
                                <tr>
                                    <th scope="row"><?php echo $heartbeat_item->id; ?></th>
                                    <td><?php echo $heartbeat_item->device_id; ?></td>
                                    <td><?php echo date('H:i:s d.m.Y', $heartbeat_item->date); ?></td>
                                    <td><?php echo $heartbeat_item->value; ?></td>
                                    <td><?php echo $heartbeat_item->time_values; ?></td>
                                    <td><?php echo $heartbeat_item->day_avg; ?></td>
                                    <td><?php echo $heartbeat_item->day_max; ?></td>
                                    <td><?php echo $heartbeat_item->day_min; ?></td>
                                    <td><?php echo $heartbeat_item->sleep_avg; ?></td>
                                    <td><?php echo $heartbeat_item->sleep_max; ?></td>
                                    <td><?php echo $heartbeat_item->sleep_min; ?></td>
                                    <td><?php echo $heartbeat_item->recent; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush