@extends('layouts.app')

@section('content')
    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Показатели</h3>
            </div>
            <div>
                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary m-btn m-btn--air m-btn--icon m-btn--icon-right m-btn--pill">
                    <span>
                        <span>Детально</span>
                        <i class="fa fa-angle-right"></i>
                    </span>
                </a>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-4">
    
                        <!--begin::Сердечный ритм-->
                        <div class="m-widget24" style="cursor: pointer;">
                            <a href="{{ route('heartbeat') }}" class="m-widget24__item">
                                <h4 class="m-widget24__title">Сердечный ритм (ЧСС)</h4><br>
                                <span class="m-widget24__link">Показать детально <i class="fa fa-angle-right"></i></span>
                                <span class="m-widget24__stats m--font-brand"><?php if(isset($heartbeat->value)): ?>{{ $heartbeat->value }} уд/мин<?php endif; ?></span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-brand" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">Плохо</span>
                                <span class="m-widget24__number">Хорошо</span>
                            </a>
                        </div>
    
                        <!--end::Сердечный ритм-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-4">
    
                        <!--begin::Артериальное давление-->
                        <div class="m-widget24" style="cursor: pointer;">
                            <a href="{{ route('pressure') }}" class="m-widget24__item">
                                <h4 class="m-widget24__title">Артериальное давление</h4><br>
                                <span class="m-widget24__link">Показать детально <i class="fa fa-angle-right"></i></span>
                                <span class="m-widget24__stats m--font-info"><?php if(!empty($pressure)): ?>{{ $pressure->systolic }}/{{ $pressure->diastolic }} мм рт.ст.<?php endif; ?></span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-info" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">Плохо</span>
                                <span class="m-widget24__number">Хорошо</span>
                            </a>
                        </div>
    
                        <!--end::Артериальное давление-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-4">
    
                        <!--begin::ЭКГ и вариативность сердца-->
                        <div class="m-widget24" style="cursor: pointer;">
                            <a href="{{ route('ecg') }}" class="m-widget24__item">
                                <h4 class="m-widget24__title">ЭКГ и вариативность сердца</h4><br>
                                <span class="m-widget24__link">Показать детально <i class="fa fa-angle-right"></i></span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">Плохо</span>
                                <span class="m-widget24__number">Хорошо</span>
                            </a>
                        </div>
    
                        <!--end::ЭКГ и вариативность сердца-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-4">
    
                        <!--begin::Физическая активность-->
                        <div class="m-widget24" style="cursor: pointer;">
                            <a href="{{ route('motions') }}" class="m-widget24__item">
                                <h4 class="m-widget24__title">Физическая активность</h4><br>
                                <span class="m-widget24__link">Показать детально <i class="fa fa-angle-right"></i></span>
                                <span class="m-widget24__stats m--font-warning">
                                    <?php if (!empty($motions)): ?>
                                    <span><?php echo $motions->step . ' ' . morph($motions->step, 'шаг', 'шага', 'шагов'); ?></span>
                                    <span>{{ $motions->distance }} км</span>
                                    <span>{{ $motions->calorie }} ккал</span>
                                    <?php endif; ?>
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-warning" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">Плохо</span>
                                <span class="m-widget24__number">Хорошо</span>
                            </a>
                        </div>
    
                        <!--end::Физическая активность-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-4">
    
                        <!--begin::Сон-->
                        <div class="m-widget24" style="cursor: pointer;">
                            <a href="{{ route('sleep') }}" class="m-widget24__item">
                                <h4 class="m-widget24__title">Сон</h4><br>
                                <span class="m-widget24__link">Показать детально <i class="fa fa-angle-right"></i></span>
                                <span class="m-widget24__stats m--font-primary">
                                <?php if (isset($sleep->time_total)): ?>
                                    <?php if (!empty($sleep->hours)) { echo $sleep->hours . ' ' . morph($sleep->hours, 'час ', 'часа ', 'часов '); } if (!empty($sleep->minutes)) { echo $sleep->minutes . ' ' . morph($sleep->minutes, 'минута', 'минуты', 'минут'); } ?>
                                <?php endif; ?>
                                </span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-primary" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">Плохо</span>
                                <span class="m-widget24__number">Хорошо</span>
                            </a>
                        </div>
    
                        <!--end::Сон-->
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-4">
    
                        <!--begin::Состояние органов брюшной полости-->
                        <div class="m-widget24" style="cursor: pointer;">
                            <a href="{{ route('heartbeat') }}" class="m-widget24__item">
                                <h4 class="m-widget24__title">Состояние органов брюшной полости</h4><br>
                                <span class="m-widget24__link">Показать детально <i class="fa fa-angle-right"></i></span>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-accent" role="progressbar" style="width: 78%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">Плохо</span>
                                <span class="m-widget24__number">Хорошо</span>
                            </a>
                        </div>
    
                        <!--end::Состояние органов брюшной полости-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush