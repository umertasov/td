@extends('layouts.app')

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Физическая активность</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-12">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Имя устройства</th>
                                    <th>Дата замера</th>
                                    <th>Кол-во калорий</th>
                                    <th>Дистанция</th>
                                    <th>Кол-во шагов</th>
                                    <th>Данные по физической активности</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($motions)): ?>
                                <?php foreach ($motions as $motion): ?>
                                <tr>
                                    <th scope="row"><?php echo $motion->id; ?></th>
                                    <td><?php echo $motion->device_id; ?></td>
                                    <td><?php echo date('H:i:s d.m.Y', $motion->date); ?></td>
                                    <td><?php echo $motion->calorie; ?></td>
                                    <td><?php echo $motion->distance; ?></td>
                                    <td><?php echo $motion->step; ?></td>
                                    <td><?php echo $motion->data; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush