@extends('layouts.app')

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Давление</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-12">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Имя устройства</th>
                                    <th>Дата замера</th>
                                    <th>Систолическое давление</th>
                                    <th>Диастолическое давление</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($pressure)): ?>
                                <?php foreach ($pressure as $pressure_item): ?>
                                <tr>
                                    <th scope="row"><?php echo $pressure_item->id; ?></th>
                                    <td><?php echo $pressure_item->device_id; ?></td>
                                    <td><?php echo date('H:i:s d.m.Y', $pressure_item->date); ?></td>
                                    <td><?php echo $pressure_item->systolic; ?></td>
                                    <td><?php echo $pressure_item->diastolic; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush