@extends('layouts.app')

@section('content')
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Сон</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-12">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Имя устройства</th>
                                    <th>Дата замера</th>
                                    <th>Общее время сна</th>
                                    <th>Длительность сна во время глубокой фазы</th>
                                    <th>Длительность сна во время легкой фазы</th>
                                    <th>Длительность пробуждения</th>
                                    <th>Фаза бодрствования</th>
                                    <th>Данные сна</th>
                                    <th>Общее время</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php if (!empty($sleep)): ?>
                                <?php foreach ($sleep as $sleep_item): ?>
                                <tr>
                                    <th scope="row"><?php echo $sleep_item->id; ?></th>
                                    <td><?php echo $sleep_item->device_id; ?></td>
                                    <td><?php echo date('H:i:s d.m.Y', $sleep_item->date); ?></td>
                                    <td><?php echo $sleep_item->time_total; ?></td>
                                    <td><?php echo $sleep_item->time_deep; ?></td>
                                    <td><?php echo $sleep_item->time_light; ?></td>
                                    <td><?php echo $sleep_item->time_stayup; ?></td>
                                    <td><?php echo $sleep_item->waking_number; ?></td>
                                    <td><?php echo $sleep_item->data; ?></td>
                                    <td><?php echo $sleep_item->total; ?></td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush

@push('scripts')

@endpush