@extends('layouts.app')

@section('content')

    <!-- BEGIN: Subheader -->
    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">Мой профиль</h3>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-12">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Ваш профиль
                            </div>

                            <div class="row">
                                <div class="m-card-profile__pic col-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="m-card-profile__pic-wrapper">
                                        <img src="<?php if (isset($user->avatar->path)) { echo $user->avatar->path; } else { echo 'img/users/default.png'; } ?>" alt="" />
                                    </div>
                                </div>

                                <div class="m-card-profile__details m-card-profile__details--offset-left col-12 col-sm-6 col-md-4 col-lg-3">
                                    <div class="m-card-profile__name"><?= $user->last_name ?> <?= $user->first_name ?></div>
                                    <div class="m-card-profile__status">Пациент</div>

                                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides m-nav--offset-top m-nav--padding-right">
                                        <li class="m-nav__section m--hide">
                                            <span class="m-nav__section-text">Действия</span>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="#" class="m-nav__link" data-toggle="modal" data-target="#modal-avatar-change">
                                                <i class="m-nav__link-icon fa fa-user-circle"></i>
                                                <span class="m-nav__link-text">Изменить фото</span>
                                            </a>
                                        </li>
                                        <li class="m-nav__item">
                                            <a href="{{ route('logout') }}" class="m-nav__link">
                                                <i class="m-nav__link-icon fa fa-sign-out-alt"></i>
                                                <span class="m-nav__link-text">Выйти из профиля</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="m-card-profile__details m-card-profile__details--family col-12 col-md-4 col-lg-6 col-md--offset-top">
                                    <span class="m-card-profile__name">
                                        <span>Моя семья</span>

                                        <span class="m-card-profile__add-family">
										    <a href="#" class="btn btn-sm btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
												<i class="fa fa-plus"></i>
											</a>
										</span>
                                    </span>

                                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides m-nav--offset-top">
                                            <li class="m-nav__section m--hide">
                                                <span class="m-nav__section-text">Действия</span>
                                            </li>
                                            <li class="m-nav__item m-nav__item--img">
                                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                                    <span class="m-nav__link-img"><img src="assets/app/media/img/users/user1.jpg" alt=""></span>
                                                    <span class="m-nav__link-text">Анастасия Иванова</span>
                                                </a>
                                            </li>
                                            <li class="m-nav__item m-nav__item--img">
                                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                                    <span class="m-nav__link-img"><img src="assets/app/media/img/users/user2.jpg" alt=""></span>
                                                    <span class="m-nav__link-text">Сергей Иванов</span>
                                                </a>
                                            </li>
                                            <li class="m-nav__item m-nav__item--img">
                                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                                    <span class="m-nav__link-img"><img src="assets/app/media/img/users/user3.jpg" alt=""></span>
                                                    <span class="m-nav__link-text">Марина Иванова</span>
                                                </a>
                                            </li>
                                            <li class="m-nav__item m-nav__item--img">
                                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                                    <span class="m-nav__link-img"><span>+5</span></span>
                                                    <span class="m-nav__link-text">Показать всех</span>
                                                </a>
                                            </li>
                                        </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link<?php if (!isset($tab) || $tab == 'profile') { echo ' active show'; } ?>" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                        <i class="flaticon-share m--hide"></i>
                                        Основная информация
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link<?php if (isset($tab) && $tab == 'biometric') { echo ' active show'; } ?>" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                        Телосложение
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link<?php if (isset($tab) && $tab == 'security') { echo ' active show'; } ?>" data-toggle="tab" href="#m_user_profile_tab_3" role="tab">
                                        Доступ
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane<?php if (!isset($tab) || $tab == 'profile') { echo ' active show'; } ?>" id="m_user_profile_tab_1">
                            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ route('edit_profile') }}">
                                @csrf
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="surname" class="col-2 col-form-label">Фамилия</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('last_name') ? ' is-invalid' : '' }}" type="text" value="<?php if (isset($user->last_name)) { echo $user->last_name; } ?>" name="last_name" id="last_name">
                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="name" class="col-2 col-form-label">Имя</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('first_name') ? ' is-invalid' : '' }}" type="text" value="<?php if (old('first_name')) { echo old('first_name'); } elseif (isset($user->first_name)) { echo $user->first_name; } ?>" name="first_name" id="first_name">
                                            @if ($errors->has('first_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('first_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="patronymic" class="col-2 col-form-label">Отчество</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('middle_name') ? ' is-invalid' : '' }}" type="text" value="<?php if (old('middle_name')) { echo old('middle_name'); } elseif (isset($user->middle_name)) { echo $user->middle_name; } ?>" name="middle_name" id="middle_name">
                                            @if ($errors->has('middle_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('middle_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="birthday" class="col-2 col-form-label">Дата рождения</label>
                                        <div class="col-7">
                                            <input type="text" class="form-control{{ $errors->has('birth_date') ? ' is-invalid' : '' }}" value="<?php if (old('birth_date')) { echo old('birth_date'); } elseif (isset($user->birth_date)) { echo $user->birth_date; } ?>" name="birth_date" id="birthday" readonly="">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="gender" class="col-2 col-form-label">Пол</label>
                                        <div class="col-7">
                                            <select class="form-control m-select2{{ $errors->has('gender') ? ' is-invalid' : '' }}" id="gender" name="gender">
                                                <option></option>
                                                <option value="0"<?php if (old('gender')) { if (old('gender') == 0) { echo 'selected'; } } elseif (isset($user->gender) && $user->gender == 0) { echo ' selected'; } ?>>Мужской</option>
                                                <option value="1"<?php if (old('gender')) { if (old('gender') == 1) { echo 'selected'; } } elseif (isset($user->gender) && $user->gender == 1) { echo ' selected'; } ?>>Женский</option>
                                            </select>
                                            @if ($errors->has('gender'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('gender') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="city" class="col-2 col-form-label">Город</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('city') ? ' is-invalid' : '' }}" value="<?php if (old('city')) { echo old('city'); } elseif (isset($user->city)) { echo $user->city; } ?>" type="text" name="city" id="city" autocomplete="off">
                                            @if ($errors->has('city'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="address" class="col-2 col-form-label">Адрес</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('address') ? ' is-invalid' : '' }}" type="text" value="<?php if (old('address')) { echo old('address'); } elseif (isset($user->address)) { echo $user->address; } ?>" name="address" id="address" autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2">
                                            </div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-brand m-btn m-btn--air m-btn--custom">Сохранить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane<?php if (isset($tab) && $tab == 'biometric') { echo ' active show'; } ?>" id="m_user_profile_tab_2">
                            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ route('edit_biometric') }}">
                                @csrf
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="height" class="col-2 col-form-label">Рост</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('height') ? ' is-invalid' : '' }}" type="text" value="<?php if (old('height')) { echo old('height'); } elseif (isset($biometric->height)) { echo $biometric->height; } ?>" name="height" id="height">
                                            @if ($errors->has('height'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('height') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="weight" class="col-2 col-form-label">Вес</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('weight') ? ' is-invalid' : '' }}" type="text" value="<?php if (old('weight')) { echo old('weight'); } elseif (isset($biometric->weight)) { echo $biometric->weight; } ?>" name="weight" id="weight">
                                            @if ($errors->has('weight'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('weight') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="hand" class="col-2 col-form-label">Рука</label>
                                        <div class="col-7">
                                            <select class="form-control m-select2{{ $errors->has('hand') ? ' is-invalid' : '' }}" id="hand" name="hand" style="width: 100%;">
                                                <option></option>
                                                <option value="0"<?php if (old('hand')) { if (old('hand') == 0) echo 'selected'; } elseif (isset($biometric->hand) && $biometric->hand == 0) echo ' selected'; ?>>Правая</option>
                                                <option value="1"<?php if (old('hand')) { if (old('hand') == 1) echo 'selected'; } elseif (isset($biometric->hand) && $biometric->hand == 1) echo ' selected'; ?>>Левая</option>
                                            </select>
                                            @if ($errors->has('hand'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('hand') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="steps" class="col-2 col-form-label">Цель шагов в день</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('goal_steps_day') ? ' is-invalid' : '' }}" type="number" value="<?php if (old('goal_steps_day')) { echo old('goal_steps_day'); } elseif (isset($biometric->goal_steps_day)) { echo $biometric->goal_steps_day; } ?>" name="goal_steps_day" id="steps">
                                        </div>
                                        @if ($errors->has('goal_steps_day'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('goal_steps_day') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="hours-sleep" class="col-2 col-form-label">Часов сна в сутки</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('goal_sleep_day') ? ' is-invalid' : '' }}" type="number" value="<?php if (old('goal_sleep_day')) { echo old('goal_sleep_day'); } elseif (isset($biometric->goal_sleep_day)) { echo $biometric->goal_sleep_day; } ?>" name="goal_sleep_day" id="hours-sleep">
                                            @if ($errors->has('goal_sleep_day'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('goal_sleep_day') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="systolic-pressure" class="col-2 col-form-label">Систолическое давление</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('systolic_pressure') ? ' is-invalid' : '' }}" type="number" value="<?php if (old('systolic_pressure')) { echo old('systolic_pressure'); } elseif (isset($biometric->systolic_pressure)) { echo $biometric->systolic_pressure; } ?>" name="systolic_pressure" id="systolic-pressure">
                                            @if ($errors->has('systolic_pressure'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('systolic_pressure') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="diastolic-pressure" class="col-2 col-form-label">Диастолическое давление</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('diastolic_pressure') ? ' is-invalid' : '' }}" type="number" value="<?php if (old('diastolic_pressure')) { echo old('diastolic_pressure'); } elseif (isset($biometric->diastolic_pressure)) { echo $biometric->diastolic_pressure; } ?>" name="diastolic_pressure" id="diastolic-pressure">
                                            @if ($errors->has('diastolic_pressure'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('diastolic_pressure') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="pulse" class="col-2 col-form-label">Пульс</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('pulse') ? ' is-invalid' : '' }}" type="number" value="<?php if (old('pulse')) { echo old('pulse'); } elseif (isset($biometric->pulse)) { echo $biometric->pulse; } ?>" name="pulse" id="pulse">
                                            @if ($errors->has('pulse'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('pulse') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2">
                                            </div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-brand m-btn m-btn--air m-btn--custom">Сохранить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane<?php if (isset($tab) && $tab == 'security') { echo ' active show'; } ?>" id="m_user_profile_tab_3">
                            <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{ route('edit_security') }}">
                                @csrf
                                <div class="m-portlet__body">
                                    <div class="form-group m-form__group row">
                                        <label for="old-pass" class="col-2 col-form-label">Старый пароль</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('password_old') ? ' is-invalid' : '' }}" type="password" value="" name="password_old" id="old-pass">
                                            @if ($errors->has('password_old'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_old') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="new-pass-1" class="col-2 col-form-label">Новый пароль</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('password_new') ? ' is-invalid' : '' }}" type="password" value="" name="password_new" id="new-pass-1">
                                            @if ($errors->has('password_new'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_new') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="new-pass-2" class="col-2 col-form-label">Повторите новый пароль</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" type="password" value="" name="password_confirmation" id="new-pass-2">
                                            @if ($errors->has('password_confirmation'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="email" class="col-2 col-form-label">Email</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" value="<?php if (old('email')) { echo old('email'); } elseif (isset($user->email)) { echo $user->email; } ?>" name="email" id="email">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <?php if (isset($user->email_verified_at) && !empty($user->email_verified_at)): ?>
                                        <div class="alert alert-success alert--no-offset" role="alert">
                                            <strong>Подтвержден</strong>
                                        </div>
                                        <?php else: ?>
                                        <a href="#" class="m-nav__link" data-toggle="modal" data-target="#modal-email-verify">
                                            <div class="alert alert-danger alert--no-offset" role="alert">
                                                <strong>Не подтвержден</strong>
                                            </div>
                                        </a>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="phone" class="col-2 col-form-label">Телефон</label>
                                        <div class="col-7">
                                            <input class="form-control m-input{{ $errors->has('phone') ? ' is-invalid' : '' }}" type="tel" value="<?php if (old('phone')) { echo old('phone'); } elseif (isset($user->phone)) { echo $user->phone; } ?>" name="phone" id="phone">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2">
                                            </div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-brand m-btn m-btn--air m-btn--custom">Сохранить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-avatar-change" tabindex="-1" role="dialog" aria-labelledby="modalAvatarChange" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Загрузите новое фото</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('upload_avatar') }}"
                              class="m-dropzone dropzone dz-clickable"
                              id="myAwesomeDropzone">
                            @csrf
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-email-verify" tabindex="-1" role="dialog" aria-labelledby="modalEmailVerify" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="emailModalLabel">{{ __('Подтверждение адреса электронной почты') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @if (session('resent'))
                            <div class="alert alert-success" role="alert">
                                {{ __('На ваш адрес электронной почты была отправлена ​​новая ссылка для подтверждения.') }}
                            </div>
                        @endif

                        {{ __('Прежде чем продолжить, проверьте свою электронную почту на наличие ссылки для подтверждения.') }}
                        {{ __('Если вы не получили письмо') }}, <a href="{{ route('verification.resend') }}">{{ __('нажмите здесь, чтобы запросить другое') }}</a>.
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/css/suggestions.min.css" type="text/css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/profile.css')}}" />
@endpush

@push('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxtransport-xdomainrequest/1.0.1/jquery.xdomainrequest.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/suggestions-jquery@18.11.1/dist/js/jquery.suggestions.min.js"></script>

    <script src="{{asset('assets/demo/base/custom.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
        $("#city").suggestions({
            token: "613ecc930331e5763b0122c87bc077334c65394e",
            type: "ADDRESS",
            hint: false,
            bounds: "city",
            constraints: {
                label: "",
                locations: { city_type_full: "город" }
            },
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });

        $("#address").suggestions({
            token: "613ecc930331e5763b0122c87bc077334c65394e",
            type: "ADDRESS",
            count: 5,
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
                console.log(suggestion);
            }
        });
    </script>

    <script>
        Dropzone.options.myAwesomeDropzone = {
            paramName: "file", // The name that will be used to transfer the file
            maxFilesize: 5, // MB
            uploadMultiple: false,
            parallelUploads: 1,
            maxFiles: 1,
            acceptedFiles: 'image/jpeg,image/png',
            dictDefaultMessage: 'Перетащите сюда файл для загрузки.',
            init: function () {
                this.on("complete", function (file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        location.reload(true);
                    }
                });
            }
        };
    </script>

    <?php if (session('status')): ?>
    <script>
        Swal.fire({
            position: 'top-end',
            type: '<?php if (isset(session('status')['result'])) { echo session('status')['result']; } else { echo 'success'; } ?>',
            title: '<?php if (isset(session('status')['message'])) { echo session('status')['message']; } ?>',
            showConfirmButton: false,
            timer: 1500
        })
    </script>
    <?php endif; ?>
@endpush