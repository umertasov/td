<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', ['uses' => 'Auth\TokenAuthController@registration']);
    Route::post('login', ['uses' => 'Auth\TokenAuthController@authenticate']);

    Route::post('reset_password/email', 'Auth\ResetPasswordController@postEmail');
    Route::post('reset_password', 'Auth\ResetPasswordController@postReset');
});

Route::prefix('user')->middleware(['token.auth'])->group(function () {
    Route::get('profile', ['uses' => 'Api\ProfileController@profile']);
    Route::post('profile', ['uses' => 'Api\ProfileController@edit']);

    Route::group(['prefix' => 'calendar'], function () {
        Route::get('{calendar_id}', ['uses' => 'Api\DrugsCalendarController@getOne']);
        Route::get('', ['uses' => 'Api\DrugsCalendarController@getAll']);
        Route::post('{calendar_id}', ['uses' => 'Api\DrugsCalendarController@edit']);
        Route::post('', ['uses' => 'Api\DrugsCalendarController@add']);
        Route::delete('{calendar_id}', ['uses' => 'Api\DrugsCalendarController@delete']);
    });

    Route::group(['prefix' => 'devices'], function () {
        Route::post('{device_id}/status', ['uses' => 'Api\Devices\StatusController@updateStatus']);

        Route::get('params', ['uses' => 'Api\Devices\DeviceParamsController@getParams']);
        Route::post('params', ['uses' => 'Api\Devices\DeviceParamsController@setParams']);

        Route::get('', ['uses' => 'Api\Devices\DeviceController@getDevices']);
        Route::post('', ['uses' => 'Api\Devices\DeviceController@addDevice']);
    });

    Route::group(['prefix' => 'measurements'], function () {
        Route::get('ecg', ['uses' => 'Api\Measurements\EcgController@getAll']);
        Route::post('ecg', ['uses' => 'Api\Measurements\EcgController@add']);

        Route::get('heartbeat', ['uses' => 'Api\Measurements\HeartbeatController@getAll']);
        Route::post('heartbeat', ['uses' => 'Api\Measurements\HeartbeatController@add']);

        Route::get('motions', ['uses' => 'Api\Measurements\MotionController@getAll']);
        Route::post('motions', ['uses' => 'Api\Measurements\MotionController@add']);

        Route::get('ppg', ['uses' => 'Api\Measurements\PpgController@getAll']);
        Route::post('ppg', ['uses' => 'Api\Measurements\PpgController@add']);

        Route::get('pressure', ['uses' => 'Api\Measurements\PressureController@getAll']);
        Route::post('pressure', ['uses' => 'Api\Measurements\PressureController@add']);

        Route::get('sleep', ['uses' => 'Api\Measurements\SleepController@getAll']);
        Route::post('sleep', ['uses' => 'Api\Measurements\SleepController@add']);
    });
});