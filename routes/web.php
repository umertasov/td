<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Auth\LoginController@showLoginForm');

Auth::routes(['verify' => true]);

Route::post('/register','Auth\AuthController@registration')->name('register');
Route::post('/login','Auth\AuthController@authenticate')->name('login');
Route::get('/logout','Auth\LoginController@logout')->name('logout');

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'profile'], function () {
    Route::post('avatar', 'ProfileController@uploadAvatar')->name('upload_avatar');
    Route::post('', 'ProfileController@edit')->name('edit_profile');
    Route::get('', 'ProfileController@profile')->name('profile');

    Route::post('biometric', 'ProfileController@editBiometric')->name('edit_biometric');
    Route::post('security', 'ProfileController@editSecurity')->name('edit_security');
});

Route::group(['prefix' => 'indicators'], function () {
    Route::get('heartbeat', 'IndicatorsController@heartbeat')->name('heartbeat');
    Route::get('pressure', 'IndicatorsController@pressure')->name('pressure');
    Route::get('ecg', 'IndicatorsController@ecg')->name('ecg');
    Route::get('motions', 'IndicatorsController@motions')->name('motions');
    Route::get('sleep', 'IndicatorsController@sleep')->name('sleep');
    Route::get('', 'IndicatorsController@indicators')->name('indicators');
});
